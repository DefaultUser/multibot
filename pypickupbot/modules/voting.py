# pypickupbot - An ircbot that helps game players to play organized games
#               with captain-picked teams.
#     Copyright (C) 2010 pypickupbot authors
#     Copyright (C) 2012-2017 Jan Behrens <zykure42@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from time import time, sleep, mktime
from datetime import datetime, timedelta
from math import floor,ceil
from email.mime.text import MIMEText
from urllib import urlopen
import sys
import httplib, smtplib
import re
import thread
import cgi, urlparse
import json

from twisted.internet import defer, reactor
from twisted.python import log
from twisted.web import server, resource

from pypickupbot.modable import SimpleModuleFactory
from pypickupbot import db
from pypickupbot import config
from pypickupbot.irc import COMMAND, InputError, FetchedList
from pypickupbot.misc import str_from_timediff, timediff_from_str,\
    InvalidTimeDiffString, StringTypes, itime



# TODO:
#   add comments!
#   check rules vor vote passing (currently: simple majority of yes/no votes, take into account userlist)
#   (maybe) add timeouts to web interface (start, end, timeout if different from end)


#######################################################################################################################

# List of allowed characters in poll names
_allowed_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 !\"#$%&'()*+,-./:;<=>?@[]^_{|}~"

# Regex for mathing URLs -> _url_regex.sub(r"<\1>", text)
_url_regex = re.compile(r"((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)")

# Regex for matching polls -> _poll_regex.sub(r"\1<\3>", text)
_poll_regex = re.compile(r"(\s+)(#([0-9]+))")


def _timestring(timestamp, useUTC=False, fromNow=True):
    """Converts a given timestamp into a human-readable duration."""
    if not timestamp:
        return ""
    if useUTC:
        return datetime.fromtimestamp(int(timestamp)).ctime()
    else:
        if fromNow:
            diff = int(timestamp - time())
            absdiff = abs(diff)
            days, rem = divmod(absdiff, 86400)
            hours, rem = divmod(rem, 3600)
            minutes, seconds = divmod(rem, 60)
            if diff < 0:
                # timestamp lies in the past
                if days > 0:
                    return "%d days, %02d:%02d:%02d ago" % (days,hours,minutes,seconds)
                else:
                    return "%02d:%02d:%02d ago" % (hours,minutes,seconds)
            elif diff > 0:
                # timestamp lies in the future
                if days > 0:
                    return "in %d days, %02d:%02d:%02d" % (days,hours,minutes,seconds)
                else:
                    return "in %02d:%02d:%02d" % (hours,minutes,seconds)
            else:
                # timestamp is now
                return "just now"
        else:
            days, rem = divmod(timestamp, 86400)
            hours, rem = divmod(rem, 3600)
            minutes, seconds = divmod(rem, 60)
            if days > 0:
                if hours + minutes + seconds == 0:
                    return "%d days" % (days)
                else:
                    return "%d days, %02d:%02d:%02d" % (days,hours,minutes,seconds)
            else:
                return "%02d:%02d:%02d" % (hours,minutes,seconds)

def _convert_time(trange):
    """Parses a duration string into an integer representing seconds.

    Format is something like "5 hours", "15 min", "10 d" etc.
    """
    fields = trange.split(" ",2)
    if len(fields) > 1:
        value, unit = float(fields[0]), fields[1].lower()
    else:
        value, unit = float(fields[0]), ""
    if unit == "weeks" or unit == "week" or unit == "wk" or unit == "w":
        return int(float(value) * 86400*7)
    elif unit == "days" or unit == "day" or unit == "dy" or unit == "d":
        return int(float(value) * 86400)
    elif unit == "hours" or unit == "hour" or unit == "hr" or unit == "h":
        return int(float(value) * 3600)
    elif unit == "minutes" or unit == "minute" or unit == "min" or unit == "mins" or unit == "m":
        return int(float(value) * 60)
    return int(value)

def _tinyurl(url):
    data = urlopen("http://tinyurl.com/api-create.php?url={0}".format(url))
    return data.read().decode().strip()

#######################################################################################################################

class Vote:
    """Class representing a single poll.

    Polls have a series of possible status flags, of which only one can
    be active at any given time. All of the poll's parameters are
    defined in a database.
    """

    VERROR          = -2    # this should not happen
    VSTOPPED        = -1    # stopped by owner
    VRUNNING        = 0     # currently running
    VFAILED         = 1     # failed
    VPASSED         = 2     # passed
    VTIED           = 3     # tied, has same amount of "yes" and "no" votes
    VVETOED         = 10    # vetoed by admin, equal to "failed" but can be challenged
    VPOSTPONED      = 11    # postponed by admin, can be continued later
    VCHALLENGED     = 12    # challenged poll, only available to admins
    VDEPRECATED     = 13    # deprectaed by re-opening of this poll
    VDECIDED        = 14    # decision, essentially a passed poll without voting

    def __init__(self, bot, index, title, timeout, creator, create_dt, end_dt, status=VRUNNING, result=(0,0)):
        self.bot        = bot
        self.index      = index
        self.title      = title
        self.timeout    = timeout
        self.creator    = creator
        self.create_dt  = create_dt
        self.end_dt     = end_dt
        self.status     = status
        self.result     = result

        self.abstained  = 0
        self.results    = []

    def _short_title(self, fixedLength=None):
        """Shorten description for display."""
        if fixedLength:
            title_length = fixedLength
        else:
            title_length = config.getint("Voting", "description shortening")
        if title_length > 0:
            title = ""
            count = 0
            for s in self.title:
                if s in " +-/@_|":
                    count += 1
                    if count >= title_length:
                        break
                title += s
            if len(title) < len(self.title):
                title += "..."
        else:
            title = self.title
        return title

    def _poll_url(self, no_markup=False, no_tinyurl=False):
        url = "{0}:{1}/poll/{2}".format(config.get("Voting", "http server url"), config.get("Voting", "http server port"), self.index)
        if not no_tinyurl:
            url = _tinyurl(url)
        if no_markup:
            return url
        return "\x0312{0}\x0f".format(url)

    def __repr__(self):
        """Short representation of this poll."""
        yesvotes, novotes = self.getResult()
        return _("\x02\x0306{0}\x0f by \x02\x0302{1}\x0f: \x0303\x02{2}\x0f:\x0304\x02{3}\x0f~\x0302\x02{4}\x0f").\
                format(self._short_title(), self.creator, yesvotes, novotes, self.abstained)

    def __str__(self):
        """Long representation of this poll."""
        yesvotes, novotes = self.getResult()
        abstained = self.abstained
        timeout = _timestring(self.getTimeout())
        reply = self._poll_url() + " "
        if self.status == self.VRUNNING:
            leftusers = len(self.bot.users.getUsers()) - (yesvotes + novotes + abstained)
            reply += _("\x02\x0306{0}\x0f by \x02\x0302{1}\x0f: \x0303\x02{2}\x0f:\x0304\x02{3}\x0f~\x0302\x02{6}\x0f \x0314({5} votes missing, ends {4})\x0f.").\
                    format(self.title, self.creator, yesvotes, novotes, timeout, leftusers, abstained)
        elif self.status == self.VCHALLENGED:
            leftusers = len(self.bot.users.getAdmins()) - (yesvotes + novotes + abstained)
            reply = _("\x02\x0306{0}\x0f by \x02\x0302{1}\x0f: \x0303\x02{2}\x0f:\x0304\x02{3}\x0f~\x0302\x02{6}\x0f \x0314(\02\x0313challenged\x0f\x0314, {5} votes missing, ends {4})\x0f.").\
                    format(self.title, self.creator, yesvotes, novotes, timeout, leftusers, abstained)
        else:
            if self.status == self.VSTOPPED:
                status = "\x0305stopped\x0f"
            elif self.status == self.VFAILED:
                status = "\x0304failed\x0f"
            elif self.status == self.VPASSED:
                status = "\x0303passed\x0f"
            elif self.status == self.VTIED:
                status = "\x0302tied\x0f"
            elif self.status == self.VVETOED:
                status = "\x0307vetoed\x0f"
            elif self.status == self.VPOSTPONED:
                status = "\x0312postponed\x0f"
            elif self.status == self.VDEPRECATED:
                status = "\x0314deprecated\x0f"
            elif self.status == self.VDECIDED:
                status = "\x0309decided\x0f"
            else:
                status = "unknown ({0})".format(self.status)
            reply = _("\x02\x0306{0}\x0f by \x02\x0302{1}\x0f: \x0303\x02{2}\x0f:\x0304\x02{3}\x0f~\x0302\x02{6}\x0f \x0314(\x02{4}\x0314, ended {5})\x0f.").\
                    format(self.title, self.creator, yesvotes, novotes, status, _timestring(self.getEnd()), abstained)
        return reply

    ##def _getResults(self):
    ##"""Retrieve list of votes on this poll from the database."""
    ##    def _doTransaction(txn):
    ##        txn.execute("""
    ##            SELECT id,name,result,comment,create_dt
    ##            FROM vote_results
    ##            WHERE
    ##                (vote_id = ?)
    ##            ORDER BY create_dt
    ##            """, (self.index,) )
    ##        self.results = []
    ##        cyes, cno, cabstain = 0, 0, 0
    ##        for (index,name,result,comment,create_dt) in txn.fetchall():
    ##            self.results.append( VoteResult(index,name,result,comment,create_dt,self.index) )
    ##            if result == VoteResult.VYES:
    ##                cyes += 1
    ##            elif result == VoteResult.VNO:
    ##                cno += 1
    ##            elif result == VoteResult.VABSTAIN:
    ##                cabstain += 1
    ##        self.result = cyes, cno
    ##        self.abstained = cabstain
    ##        log.msg("Updated vote {0}: {1} votes".format(self.index, len(self.results)))
    ##    return db.runInteraction(_doTransaction)

    def getStart(self):
        """Determine starting date of this poll."""
        if self.create_dt > 0:
            return self.create_dt
        return None

    def getTimeout(self):
        """Determine timeout date of this poll."""
        try:
            return self.create_dt + self.timeout
        except:
            return None

    def getEnd(self):
        """Determine end date of this poll.

        Can be different from timeout date if the poll ended by having
        enough "yes" or "no" votes.
        """
        if self.end_dt > 0:
            return self.end_dt
        return None

    def getLength(self):
        """Determine total running time of this poll."""
        try:
            return self.end_dt - self.create_dt
        except:
            return None

    def isActive(self, curtime=None):
        """Determine if this poll is active (has started and not yet ended)."""
        if not curtime:
            curtime = itime()
        if not self.getEnd():
            return curtime >= self.getStart()
        return curtime >= self.getStart() and curtime < self.getEnd()

    def isOverdue(self, curtime=None):
        """Determine if this poll is overdue (has reached its timeout date).


        This case can happen if the poll is (re-)loaded from the
        database and timed out in the meantime.
        """
        if not curtime:
            curtime = itime()
        if not self.getTimeout():
            return False
        return curtime >= self.getStart() and curtime > self.getTimeout()

    def isFinished(self, curtime=None):
        if not curtime:
            curtime = itime()
        return curtime >= self.getStart() and curtime > self.getEnd()

    def isRunningout(self, window, curtime=None):
        """Determine if this poll is running out soon.

        The meaning of "soon" is defined by the given "window" parameter.
        """
        if not curtime:
            curtime = itime()
        if not self.getTimeout():
            return False
        return curtime >= self.getStart() and curtime > (self.getTimeout() - window)

    def isRunning(self):
        """Determine if this poll is currently running (by status flag, NOT by time)."""
        return (self.status in [ self.VRUNNING, self.VCHALLENGED ])

    def isClosed(self):
        return (self.status in [ self.VSTOPPED, self.VFAILED, self.VPASSED, self.VTIED, self.VVETOED, self.VDEPRECATED ])

    def isPostponed(self):
        return (self.status in [ self.VPOSTPONED ])

    def beenStopped(self):
        return (self.status == self.VSTOPPED) and self.isFinished()

    def hasPassed(self):
        return (self.status == self.VPASSED) and self.isFinished()

    def hasFailed(self):
        return (self.status == self.VFAILED) and self.isFinished()

    def hasTied(self):
        return (self.status == self.VTIED) and self.isFinished()

    def beenVetoed(self):
        return (self.status == self.VVETOED) and self.isFinished()

    def beenDeprecated(self):
        return (self.status == self.VDEPRECATED) and self.isFinished()

    def isDecided(self):
        return (self.status == self.VDECIDED) and self.isFinished()

    def getVotes(self):
        """Return a dictionary of votes (containing name:vote pairs)."""
        votes = {}
        for vote_result in self.results:
            votes[vote_result.name] = vote_result
        return votes

    def getCreator(self):
        """Return the creator of this poll."""
        return self.creator

    def getStatus(self):
        """Return the status of this poll."""
        return self.status

    def getResult(self):
        """Return the result of this poll (a tuple of the number of (yes,no) votes)."""
        return self.result

    def getAbstained(self):
        """Return the number of abstained votes on this poll."""
        return self.abstained

    def getVetoers(self):
        """Return a list of users who vetoed this poll."""
        users = []
        for result in self.results:
            if result.result == VoteResult.VVETO:
                users.append(result.name)
        return users

    def getApprovers(self):
        """Return a list of users who voted "yes" on this poll."""
        users = []
        for result in self.results:
            if result.result == VoteResult.VYES:
                users.append(result.name)
        return users

    def getDisapprovers(self):
        """Return a list of users who voted "no" on this poll."""
        users = []
        for result in self.results:
            if result.result == VoteResult.VNO:
                users.append(result.name)
        return users

    def getAbstainers(self):
        """Return a list of users who voted "abstain" on this poll."""
        users = []
        for result in self.results:
            if result.result == VoteResult.VABSTAIN or result.result == VoteResult.VAUTOABSTAIN:
                users.append(result.name)
        return users

#######################################################################################################################

class VoteResult:
    """Class representing a single vote on a given poll.

    A vote is defined by one status flag. A vote also knows the poll
    it's belonging to.
    """

    VAUTOABSTAIN    = -1
    VABSTAIN        = 0
    VYES            = 1
    VNO             = 2
    VVETO           = 3

    def __init__(self, index, name, result, comment, create_dt, vote_index):
        self.index      = index
        self.name       = name
        self.result     = result
        self.comment    = comment
        self.create_dt  = create_dt
        self.vote_index = vote_index

    def getResult(self):
        if self.result == self.VABSTAIN or self.result == self.VAUTOABSTAIN:
            result = _("abstain")
        elif self.result == self.VYES:
            result = _("yes")
        elif self.result == self.VNO:
            result = _("no")
        elif self.result == self.VVETO:
            result = _("veto")
        else:
            result = _("invalid ({0})").format(self.result)
        return result

    def __repr__(self):
        timestamp = ""
        result = self.getResult()
        if self.comment:
            return "{0}: {1} - {2}".format(self.name, result, self.comment)
        else:
            return "{0}: {1}".format(self.name, result)

    def __str__(self):
        """Short representation of this vote."""
        timestamp = ""
        if self.result == self.VABSTAIN or self.result == self.VAUTOABSTAIN:
            result = _("\x0302abstain\x0f")
        elif self.result == self.VYES:
            result = _("\x0303yes\x0f")
        elif self.result == self.VNO:
            result = _("\x0304no\x0f")
        elif self.result == self.VVETO:
            result = _("\x0307veto\x0f")
        else:
            result = _("\x0finvalid ({0})").format(self.result)
        timestamp = _timestring(self.create_dt)
        return _("\x02\x0302{0}\x0f voted \x02{1}\x0f \x0314(at {2}, {3})\x0f").\
                format(self.name, result, timestamp,
                    "comment: '{0}'".format(self.comment) if len(self.comment) > 0 else "no comment")


#######################################################################################################################

class VotingSystem:
    """The interface to the SQLite DB, used to load and store polls."""

    def __init__(self, bot, cback=None):
        self.bot    = bot
        self.votes  = {}       # id : Vote

        ## FIXME - (vote_id) should reference (id) of the votes table!
        ## FIXME - also (vote_id,name) should be unique!
        def _doTransaction(txn):
            txn.execute("""
                CREATE TABLE IF NOT EXISTS
                votes
                (
                    id          INTEGER PRIMARY KEY AUTOINCREMENT,
                    title       TEXT,
                    timeout     INTEGER,
                    creator     TEXT,
                    create_dt   INTEGER,
                    end_dt      INTEGER,
                    status      INTEGER,
                    vyes        INTEGER,
                    vno         INTEGER
                )""")
            txn.execute("""
                CREATE TABLE IF NOT EXISTS
                vote_results
                (
                    id          INTEGER PRIMARY KEY AUTOINCREMENT,
                    vote_id     INTEGER,
                    name        TEXT,
                    result      INTEGER,
                    comment     TEXT,
                    create_dt   INTEGER
                )""")
            return txn.fetchall()

            ## FIXME  - THIS NEEDS REWORK!

        def _done(args):
            def _done(args):
                if cback:
                    self._load_results_from_db().addCallback(cback)
                else:
                    self._load_results_from_db()
            self._load_from_db().addCallback(_done)
        db.runInteraction(_doTransaction).addCallback(_done)

    def reload(self, cback=None):
        """Reload all polls from the DB."""
        def _done(args):
            if cback:
                self._load_results_from_db().addCallback(cback)
            else:
                self._load_results_from_db()
        self._load_from_db().addCallback(_done)

    def __getitem__(self, index):
        try:
            return self.votes[index]
        except:
            return None

    def _load_results_from_db(self):
        """used by this class to load all single votes from the db."""
        d = db.runQuery("""
            SELECT id,vote_id,name,result,comment,create_dt
            FROM vote_results
            ORDER BY id
            """)
        def _loadResults(r):
            # clear old table
            for entry in r:
                index, vote_id, name, result, comment, create_dt = entry
                if not vote_id in self.votes:
                    continue
                vote = self.votes[vote_id]
                vote.results = []
                vote.result = 0,0
                vote.abstained = 0
            # fill in data from the db
            for entry in r:
                index, vote_id, name, result, comment, create_dt = entry
                if not vote_id in self.votes:
                    continue
                vote = self.votes[vote_id]
                vote.results.append( VoteResult(index,name,result,comment,create_dt,index) )
                cyes, cno = vote.result
                cabstain = vote.abstained
                if result == VoteResult.VYES:
                    cyes += 1
                elif result == VoteResult.VNO or result == VoteResult.VVETO:
                    cno += 1
                elif result == VoteResult.VABSTAIN or result == VoteResult.VAUTOABSTAIN:
                    cabstain += 1
                vote.result = cyes, cno
                vote.abstained = cabstain
        return d.addCallback(_loadResults)

    def _load_from_db(self):
        """used by this class to load all polls from the db, without any votes."""
        d = db.runQuery("""
            SELECT      id, title, timeout, creator, create_dt, end_dt, status, vyes, vno
            FROM        votes
            ORDER BY    id
            """)
        self.votes = {}
        def _loadVotes(r):
            for entry in r:
                index, title, timeout, creator, create_dt, end_dt, status, vyes, vno = entry
                vote = Vote(self.bot, index, title, timeout, creator, create_dt, end_dt, status, (vyes,vno))
                self.votes[index] = vote
        return d.addCallback(_loadVotes)

    #def _delete(self, index):
    #    """delete a given poll from the db."""
    #    return db.runOperation("""
    #        DELETE FROM votes
    #        WHERE       id = :index;
    #        DELETE FROM vote_results
    #        WHERE       vote_id = :index
    #        """, (index,))

    def _insert_vote(self, title, timeout, creator, status=Vote.VRUNNING):
        """add poll."""
        return db.runOperation("""
            INSERT INTO votes(title, timeout, creator, create_dt, end_dt, status, vyes, vno)
            VALUES      (:title, :timeout, :creator, :create_dt, :end_dt, :status, :vyes, :vno)
            """, (title, timeout, creator, itime(), 0, status, 0, 0))

    def _update_vote(self, index, timeout, end_dt, status, result):
        """change given poll."""
        return db.runOperation("""
            UPDATE votes
            SET    timeout= :timeout,
                   end_dt = :end_dt,
                   status = :status,
                   vyes   = :vyes,
                   vno    = :vno
            WHERE  id = :id
            """, (timeout, end_dt, status, result[0], result[1], index))

    def _rename_vote(self, index, title):
        """rename given poll."""
        return db.runOperation("""
            UPDATE votes
            SET    title= :title
            WHERE  id = :id
            """, (title, index))

    def _insert_result(self, index, name, result, comment):
        """add single vote for given poll."""
        return db.runOperation("""
            INSERT INTO vote_results(vote_id, name, result, comment, create_dt)
            VALUES      (:vote_id, :name, :result, :comment, :create_dt)
            """, (index, name, result, comment, itime(),))

    def _insert_result_multi(self, index, names, result, comment):
        """add multiple votes for given poll."""
        def _doTransaction(txn, index, names, result, comment):
            for name in names:
                txn.execute("""
                    INSERT INTO vote_results(vote_id, name, result, comment, create_dt)
                    VALUES      (:vote_id, :name, :result, :comment, :create_dt)
                    """, (index, name, result, comment, itime(),))
            txn.fetchall()
        return db.runInteraction(_doTransaction, index, names, result, comment)

    def _update_result(self, index, name, result, comment):
        """change single vote for given poll."""
        return db.runOperation("""
            UPDATE vote_results
            SET    result    = :result,
                   comment   = :comment,
                   create_dt = :create_dt
            WHERE  vote_id = :vote_id AND name = :name
            """, (result, comment, itime(), index, name,))


#######################################################################################################################

class User:
    """Class representing a single user.

    This class is employed to check access rights for voting and
    administering polls.
    """

    UINVALID    = -1    # invalid user, isn't allowed to do anything
    UNORMAL     = 0     # normal user, can start and change own polls, and vote on other's polls
    UPRIVILEGED = 1     # privileged user, can change other's polls and veto/challenge any poll

    def __init__(self, index, name, authname, status, create_dt):
        self.index      = index
        self.name       = name
        self.authname   = authname
        self.status     = status
        self.create_dt  = create_dt

    def __str__(self):
        """Short representation of this user."""
        if self.isOperator():
            return _("\x02{0}\x02* (ident: {1})").format(self.name, self.authname)
        else:
            return _("\x02{0}\x02 (ident: {1})").format(self.name, self.authname)

    def isOperator(self):
        """Check if this user has privileged access rights."""
        return self.status >= self.UPRIVILEGED

    def isValid(self):
        """Check if this user has any access rights."""
        return self.status >= self.UNORMAL

#######################################################################################################################

class UserList:
    """Interface to the user DB."""

    def __init__(self, bot, cback=None):
        self.bot = bot
        self.users = {}         # users in DB - authname : user

        # FIXME - at least authname should be unique!
        d = db.runOperation("""
            CREATE TABLE IF NOT EXISTS
            users
            (
                id          INTEGER PRIMARY KEY AUTOINCREMENT,
                user        TEXT,
                authname    TEXT,
                status      INTEGER,
                create_dt   INTEGER
            )""")
        def _done(args):
            if cback:
                self._load_from_db().addCallback(cback)
            else:
                self._load_from_db()
        d.addCallback(_done)

    def reload(self, cback=None):
        """Reload all users from the db."""
        if cback:
            self._load_from_db().addCallback(cback)
        else:
            self._load_from_db()

    def _load_from_db(self):
        """used by this class to load all users."""
        d = db.runQuery("""
            SELECT      id, user, authname, status, create_dt
            FROM        users
            ORDER BY    id
            """)
        self.users = {}
        def _loadUsers(r):
            for entry in r:
                index, user, authname, status, create_dt = entry
                self.users[authname] = User(index, user, authname, status, create_dt)
        return d.addCallback(_loadUsers)

    #def _remove(self, index):
    #    """Remove entry with given id."""
    #    return db.runOperation("""
    #        DELETE FROM users
    #        WHERE       id = :index;
    #        """, (index,))

    #def _del_user(self, name):
    #    """Remove given user."""
    #    return db.runOperation("""
    #        DELETE FROM users
    #        WHERE       authname = :name;
    #        """, (name,))

    def _add_user(self, user, authname, status):
        """Add given user."""
        return db.runOperation("""
            INSERT INTO users(user, authname, status, create_dt)
            VALUES      (:user, :authname, :status, :create_dt)
            """, (user, authname, status, itime(),))

    def _modify_user(self, authname, status):
        """Modify given user."""
        return db.runOperation("""
            UPDATE users
            SET    status = :status
            WHERE  authname = :authname
            """, ( authname, status, ))

    def isAllowed(self, authname, restricted=False):
        """Check if user is known (authname exists).

        If "restricted" is set, also check if the user has privileged access.
        """
        if not self.users.has_key(authname):
            return False
        if restricted:
            if not self.users[authname].isOperator():
                return False
        return self.users[authname].isValid()

    def getUsers(self):
        """Get a dictionary of users (containing authname:User pairs)."""
        users = {}
        for u in self.users.values():
            if u.isValid():
                users[u.authname] = u
        return users

    def getAdmins(self):
        """Get a dictionary of priviliged users (equivalent to getUsers)."""
        users = {}
        for u in self.users.values():
            if u.isOperator():
                users[u.authname] = u
        return users

#######################################################################################################################

class VoteHTTPServer(resource.Resource):
    """Simple HTTP server to publish polls and users over the WWW.

    The webserver is built on Twisted's Resource class, and provides
    views of all polls, including all votes on any poll, and a list
    of all defined users.
    """

    isLeaf = True

    # Global HTML structure
    htmlOutline = """
<html>
    <head>
        <title>%(title)s</title>
        <link href='//fonts.googleapis.com/css?family=%(webfont)s' rel='stylesheet'>
        <style type="text/css">
            body {
                background: %(bgcolor)s;
                color: %(fgcolor1)s;
                margin: 2em 10%%;
                font-family: %(webfont)s,sans-serif;
                font-size: 12pt;
            }
            a {
                text-decoration: none;
                color: %(urlcolor)s;
            }
            table {
                width: 100%%;
                border: 1px solid %(bordercolor)s;
                color: %(textcolor)s;
                font-size: 10pt;
                margin: 0;
                padding: 2pt;
                border-spacing: 0;
                border-collapse: collapse;
            }
            table th {
                border: 1px solid %(bordercolor)s;
                background-color: %(bordercolor)s;
                color: %(fgcolor2)s;
                padding: 5pt;
                text-align: left;
            }
            table td {
                border: 1px solid %(bordercolor)s;
                padding: 5pt;
                white-space: nowrap;
            }
            table td.title {
                white-space: normal;
                //overflow: hidden;
                //text-overflow: ellipsis;
            }
            table tr {
                background-color: %(tablecolor)s;
            }
            table tr:hover td {
                background-color: %(hovercolor)s;
            }
            table.tablesorter thead tr .header {
                cursor: pointer;
            }
            table.tablesorter thead tr .header:before {
                content: '';
            }
            table.tablesorter thead tr .headerSortUp:before {
                content: '\\25B4  ';
            }
            table.tablesorter thead tr .headerSortDown:before {
                content: '\\25BE  ';
            }
            div.info {
                margin: 2em 3em;
                border: 1px solid %(bordercolor)s;
                background-color: %(tablecolor)s;
                color: %(textcolor)s;
                padding: 5pt;
            }
            div.info .field {
                font-weight: bold;
                width: 10em;
                display: inline-block;
            }
            #navbar {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%%;
                background-color: %(bordercolor)s;
                color: %(fgcolor2)s;
                padding: 5pt;
                font-weight: bold;
                font-size: 11pt;
            }
            #navbar .tab {
                width: 50%%;
                display: inline;
                margin: 2px 25px;
            }
            #navbar .tab div {
                width: 20%%;
                color: %(fgcolor1)s;
                display: inline;
            }
            #navbar a,
            #navbar a:visited {
                color: %(fgcolor1)s;
                text-align: center;
                text-decoration: none;
                padding: 0 5px;
            }
            #navbar a:active,
            #navbar a:hover {
                color: %(fgcolor2)s;
            }
            #navbar a.rss {
                color: %(fgcolor1)s;
                background-color: #555;
                padding: 0pt 3pt;
                border-radius: 2px;
            }
            #navbar a.rss:hover {
                background-color: #f60;
                color: %(fgcolor2)s;
            }
        </style>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <!--script type="text/javascript" src="http://www.xonotic-gaming.org/_files/jquery.tablesorter.min.js"></script-->
        <script type="text/javascript">
            %(script)s
        </script>
        <link rel="alternate" type="application/rss+xml" title="RSS Feed" href="/feed"/>
    </head>
    <body>
        <div id="navbar">
            <div class="tab" style="margin: 0;">
                <a href="%(baseurl)s/feed" class="rss">RSS</a>
            </div>
            <div class="tab">
                <a href="%(baseurl)s/" style="color:#fff;">xonvote!</a>
            </div>
            <div class="tab">
                <div><a href="%(baseurl)s/polls?%(query)s">Polls</a> &#x25b8;</div>
                <div><a href="%(baseurl)s/polls/running?%(query)s">running</a></div>
                <div><a href="%(baseurl)s/polls/ending?%(query)s">ending</a></div>
                <div><a href="%(baseurl)s/polls/closed?%(query)s">closed</a></div>
                <div><a href="%(baseurl)s/polls/passed?%(query)s">passed</a></div>
                <div><a href="%(baseurl)s/polls/failed?%(query)s">failed</a></div>
                <div><a href="%(baseurl)s/polls/tied?%(query)s">tied</a></div>
                <div><a href="%(baseurl)s/polls/vetoed?%(query)s">vetoed</a></div>
            </div>
            <div class="tab">
                <div><a href="%(baseurl)s/users?%(query)s">Users</a></div>
            </div>
        </div>
        <br/>

        %(body)s
    </body>
</html>
"""

    rssOutline = """<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/">
    <channel>
        <title>%(title)s</title>
        <description>%(description)s</description>
        <link>%(baseurl)s/%(link)s</link>
        <language>en</language>
        <docs>http://blogs.law.harvard.edu/tech/rss</docs>
        <lastBuildDate>%(timestamp)s</lastBuildDate>
        <pubDate>%(timestamp)s</pubDate>
        <ttl>%(ttl)d</ttl>

        %(items)s

    </channel>
</rss>
"""

    def __init__(self, bot):
        self.votebot = bot
        self.accesskey = config.get("Voting", "access key").decode('string-escape')
        self.site = server.Site(self)
        self.handle = reactor.listenTCP(int(config.get("Voting", "http server port").decode('string-escape')), self.site)

    #------------------------------------------------------------------

    def render_GET(self, request):
        #params = request.uri.split("/")

        split = urlparse.urlsplit(request.uri)
        attrs = urlparse.parse_qs(split.query)

        public_view = True
        if attrs.has_key("key"):
            key = attrs["key"][0].strip()
            if key == self.accesskey:
                public_view = False

        params = [ x for x in split.path.split("/") if x ]  # combine adjacent separators
        uri = "/".join(params[:1])  # params[1:] -> actual URL params
        baseurl = url = "{0}:{1}".format(config.get("Voting", "http server url"), config.get("Voting", "http server port"))

        #print request.uri, "->", split, attrs, "->", uri, params
        #print "public_view", public_view

        if uri in [ "polls", "votes" ]:
            if len(params) >= 2:
                if params[1] == "closed":
                    return str(self.print_polls( baseurl=baseurl, query=split.query, public_view=public_view, show_closed=True ))
                elif params[1] == "running":
                    return str(self.print_polls( baseurl=baseurl, query=split.query, public_view=public_view, show_running=True ))
                elif params[1] == "ending":
                    return str(self.print_polls( baseurl=baseurl, query=split.query, public_view=public_view, show_ending=True ))
                elif params[1] == "passed":
                    return str(self.print_polls( baseurl=baseurl, query=split.query, public_view=public_view, show_passed=True ))
                elif params[1] == "failed":
                    return str(self.print_polls( baseurl=baseurl, query=split.query, public_view=public_view, show_failed=True ))
                elif params[1] == "tied":
                    return str(self.print_polls( baseurl=baseurl, query=split.query, public_view=public_view, show_tied=True ))
                elif params[1] == "vetoed":
                    return str(self.print_polls( baseurl=baseurl, query=split.query, public_view=public_view, show_vetoed=True ))
            return str(self.print_polls( baseurl=baseurl, query=split.query, public_view=public_view ))

        if uri in [ "users" ]:
            return str(self.print_users( baseurl=baseurl, query=split.query, public_view=public_view ))

        if uri in [ "rss", "feed" ]:
                request.setHeader("Content-Type", "application/rss+xml; charset=utf-8")
                #request.setHeader("Content-Type", "text/xml")
                return str(self.print_rss( baseurl=baseurl ))

        if uri in [ "poll", "vote"]:
            if len(params) >= 2:
                return str(self.print_poll_details( int(params[1]), baseurl=baseurl, query=split.query, public_view=public_view ))

        if uri == "robots.txt":
            return """
User-agent: *
Disallow: /
"""

        return self.htmlOutline % {
                'title':        config.get("Voting", "weblist title"),
                'bgcolor':      config.get("Voting", "weblist background" if public_view else "weblist background restricted"),
                'fgcolor1':     config.get("Voting", "weblist color 1"),
                'fgcolor2':     config.get("Voting", "weblist color 2"),
                'textcolor':    config.get("Voting", "weblist text color"),
                'bordercolor':  config.get("Voting", "weblist border color"),
                'tablecolor':   config.get("Voting", "weblist table color"),
                'hovercolor':   config.get("Voting", "weblist hover color"),
                'urlcolor':     config.get("Voting", "weblist link color"),
                'webfont':      config.get("Voting", "weblist font"),
                'body':         "",
                'script':       "",
                'baseurl':      baseurl,
                'query':        split.query,
            }

    #------------------------------------------------------------------

    def print_rss(self, baseurl=""):
        """Print RSS feed of all polls."""
        item_template = """
<item>
    <title>%(title)s</title>
    <description>%(description)s</description>
    <link>%(link)s</link>
    <guid>%(guid)s</guid>
    <pubDate>%(timestamp)s</pubDate>
    <content:encoded><![CDATA[ %(content)s ]]></content:encoded>
</item>
"""

        ## TODO - add config options

        users = self.votebot.users.getUsers()
        admins = self.votebot.users.getAdmins()

        items = {}  # timestamp - item

        all_votes = sorted(self.votebot.voting.votes.values(), key=lambda x: x.index)
        curtime = itime()
        for v in all_votes:
            yesvotes, novotes = v.getResult()
            abstained = v.abstained
            leftvotes = 0
            numvotes = yesvotes + novotes + abstained
            begin = _timestring(v.getStart())
            end = _timestring(v.getEnd())
            title = cgi.escape(v.title)

            if not v.isActive(curtime):
                if v.status == Vote.VRUNNING:
                    #leftvotes = len(users) - numvotes
                    #status = 'running'
                    #statuscolor = '#CC0'
                    #end = _timestring(v.getTimeout())
                    #timestamp = v.getStart()
                    continue  # skip running polls
                elif v.status == Vote.VCHALLENGED:
                    #leftvotes = len(admins) - numvotes
                    #status = 'challenged'
                    #statuscolor = '#C0C'
                    #end = _timestring(v.getTimeout())
                    #timestamp = v.getStart()
                    continue  # skip running polls
                if v.status == Vote.VSTOPPED:
                    #status = 'stopped'
                    #statuscolor = '#CCC'
                    #timestamp = v.getEnd()
                    continue  # skip stopped polls
                elif v.status == Vote.VFAILED:
                    status = 'failed'
                    statuscolor = '#C00'
                    timestamp = v.getEnd()
                elif v.status == Vote.VPASSED:
                    status = 'passed'
                    statuscolor = '#0C0'
                    timestamp = v.getEnd()
                elif v.status == Vote.VTIED:
                    status = 'tied'
                    statuscolor = '#00C'
                    timestamp = v.getEnd()
                elif v.status == Vote.VVETOED:
                    status = 'vetoed'
                    statuscolor = '#C80'
                    timestamp = v.getEnd()
                elif v.status == Vote.VPOSTPONED:
                    status = 'postponed'
                    statuscolor = '#80C'
                    timestamp = v.getEnd()
                elif v.status == Vote.VDEPRECATED:
                    #status = 'deprecated'
                    #statuscolor = '#C08'
                    #timestamp = v.getEnd()
                    continue  # skip deprecated (challenged) polls
                else:
                    #status = "unknown"
                    continue  # just skip this one

                if yesvotes > novotes:
                    color = '#0A0'
                elif yesvotes < novotes:
                    color = '#A00'
                else:
                    color = '#00A'

                pretty_title = '<span style="color:{1}">{0}</span> - <b><span style="color:{3}">{2}</span></b>'.format(title, color, status, statuscolor)
                pretty_results = '<span style="color:#0C0"><b>{0}</b></span> yes, <span style="color:#C00"><b>{1}</b></span> no, <span style="color:#00C"><b>{2}</b></span> abstained'.format(yesvotes, novotes, abstained)

                items[timestamp] = item_template % {
                        'title':        "{0}. ({2}) {1}".format(v.index, title, status),
                        'description':  "Poll by {0}. Results: {1} yes, {2} no, {3} abstained, {4} have not voted yet.".format(v.creator, yesvotes, novotes, abstained, leftvotes),
                        'content':      "{0}. Poll by {1}. Results: {2}.".format(pretty_title, v.creator, pretty_results),
                        'link':         "{0}/poll/{1}".format(baseurl, v.index),
                        'guid':         timestamp,
                        'timestamp':    datetime.fromtimestamp(int(timestamp)).strftime("%a, %d %b %Y %H:%M:%S %Z"),
                    }

            # add info on all new polls

            #timestamp = v.getStart()
            #items[timestamp] = item_template % {
            #        'title': "{0}. ({2}) {1}".format(v.index, title, "new"),
            #        'description': "New poll by {0}.".format(v.creator),
            #        'content': "{0}. Poll by {2}.".format(title, status, v.creator),
            #        'link': "",#"poll/{0}".format(v.index),
            #        'guid': timestamp,
            #        'timestamp': datetime.fromtimestamp(int(timestamp)).strftime("%a, %d %b %Y %H:%M:%S %Z"),
            #    }

        maxitems = config.getint("Voting", "rss feed items")
        body = "\n".join([ items[k] for k in sorted(items.keys(), reverse=True)[:maxitems] ])

        return self.rssOutline % {
                'title':     "xonvote! | RSS Feed",
                'description':     "The xonvote! RSS feed: Check #xonotic on freenode IRC for more info.",
                'link':     "/polls",
                'baseurl':    baseurl,
                'timestamp':    datetime.now().strftime("%a, %d %b %Y %H:%M:%S %Z"),
                'ttl':         1800,
                'items':    body,
            }

    def print_polls(self, baseurl="", query="", public_view=True, show_closed=False, show_running=False, show_ending=False, show_passed=False, show_failed=False, show_tied=False, show_vetoed=False):
        """Print list of active and closed polls, using two tables."""

        shown_votes = []
        all_votes = sorted(self.votebot.voting.votes.values(), key=lambda x: x.index)
        pretimeout = _convert_time(config.get("Voting", "pre-timeout window").encode("string-escape"))
        curtime = itime()

        for vote in all_votes:
            if show_running:
                if vote.isActive(curtime):
                    if vote.isRunning() or vote.isPostponed():
                        shown_votes.append(vote)
            elif show_ending:
                if vote.isRunningout(pretimeout, curtime):
                    if vote.isRunning() or vote.isPostponed():
                        shown_votes.append(vote)
            elif show_closed:
                if vote.isFinished(curtime):
                    if vote.isClosed():
                        shown_votes.append(vote)
            elif show_passed:
                if vote.isFinished(curtime):
                    if vote.hasPassed() or vote.isDecided():
                        shown_votes.append(vote)
            elif show_failed:
                if vote.isFinished(curtime):
                    if vote.hasFailed():
                        shown_votes.append(vote)
            elif show_tied:
                if vote.isFinished(curtime):
                    if vote.hasTied():
                        shown_votes.append(vote)
            elif show_vetoed:
                if vote.isFinished(curtime):
                    if vote.beenVetoed() or vote.beenDeprecated():
                        shown_votes.append(vote)
            else:
                shown_votes.append(vote)

        table_template = """
<table class="tablesorter" id="%(id)s">
    <thead>
        <tr>
            <th style="width:2em">#</th>
            <th style="width:auto">Title</th>
            <th style="width:5em">Owner</th>
            <th style="width:5em">Votes</th>
            <th style="width:10em">Result</th>
            <th style="width:5em">Status</th>
            <th style="width:10em">Start</th>
            <th style="width:10em">End</th>
        </tr>
    </thead>
    <tbody>
        %(content)s
    </tbody>
</table>
"""

        table_entry = """
<tr>
    <td><a href="%(baseurl)s/poll/%(id)d?%(query)s">%(id)d.</a></td>
    <td class="title" title="%(title)s">%(title_html)s</td>
    <td><i>%(creator)s</i></td>
    <td><b>%(numvotes)d</b> (%(leftvotes)d left)</td>
    <td><span style="color:#0c0"><b>%(yesvotes)d</b></span> : <span style="color:#c00"><b>%(novotes)d</b></span> (<span style="color:#00c"><b>%(abstained)d</b></span> abstained)</td>
    <td><b>%(status)s</b></td>
    <td title="%(begin_dt)s">%(begin)s</td>
    <td title="%(end_dt)s">%(end)s</td>
</tr>
"""
        table_entry_restricted = """
<tr>
    <td><a href="%(baseurl)s/poll/%(id)d?%(query)s">%(id)d.</a></td>
    <td class="title" title="%(title)s">%(title_html)s</td>
    <td><i>%(creator)s</i></td>
    <td><b>%(numvotes)d</b> (%(leftvotes)d left)</td>
    <td>N/A</td>
    <td><b>%(status)s</b></td>
    <td title="%(begin_dt)s">%(begin)s</td>
    <td title="%(end_dt)s>%(end)s</td>
</tr>
"""

        table_shown_votes = []
        users = self.votebot.users.getUsers()
        admins = self.votebot.users.getAdmins()
        for v in shown_votes:
            yesvotes, novotes = v.getResult()
            abstained = v.abstained
            leftvotes = 0
            numvotes  = yesvotes + novotes + abstained
            title     = cgi.escape(v.title)

            begin = v.getStart()
            end   = v.getEnd()

            if v.status == Vote.VRUNNING:
                leftvotes = len(users) - numvotes
                status = '<span style="color:#CC0">running</span>'
                end = v.getTimeout()
            elif v.status == Vote.VCHALLENGED:
                leftvotes = len(admins) - numvotes
                status = '<span style="color:#C0C">challenged</span>'
                end = v.getTimeout()
            elif v.status == Vote.VSTOPPED:
                status = '<span style="color:#CCC">stopped</span>'
            elif v.status == Vote.VFAILED:
                status = '<span style="color:#C00">failed</span>'
            elif v.status == Vote.VPASSED:
                status = '<span style="color:#0C0">passed</span>'
            elif v.status == Vote.VTIED:
                status = '<span style="color:#00C">tied</span>'
            elif v.status == Vote.VVETOED:
                status = '<span style="color:#C80">vetoed</span>'
            elif v.status == Vote.VPOSTPONED:
                status = '<span style="color:#80C">postponed</span>'
            elif v.status == Vote.VDEPRECATED:
                status = '<span style="color:#C08">deprecated</span>'
            elif v.status == Vote.VDECIDED:
                status = '<span style="color:#0C8">decided</span>'
            else:
                status = "unknown"
            ##status += " (#%d)" % v.status

            title_markup = title
            title_markup = _url_regex.sub(r'<a href="\1">\1</a>', title_markup)
            title_markup = _poll_regex.sub(r'\1<a href="{0}\3?{1}">\2</a>'.format(baseurl, query), title_markup)

            if not public_view or v.isClosed():
                table_shown_votes.insert(0, table_entry % {
                        'id':           v.index,
                        'baseurl':      baseurl,
                        'query':        query,
                        'title':        cgi.escape(title, quote=True),
                        'title_html':   title_markup,  # replaced URLs with HTML links
                        'creator':      v.creator,
                        'numvotes':     int(numvotes),
                        'leftvotes':    int(leftvotes),
                        'yesvotes':     int(yesvotes),
                        'novotes':      int(novotes),
                        'abstained':    int(abstained),
                        'status':       status,
                        'begin':        _timestring(begin),
                        'end':          _timestring(end),
                        'begin_dt':     _timestring(begin, useUTC=True),
                        'end_dt':       _timestring(end, useUTC=True),
                    })
            else:
                pass

        body = ""
        if len(shown_votes) > 0:
            title = _("All")
            if show_running:
                title = _("Running")
            elif show_ending:
                title = _("Ending")
            if show_closed:
                title = _("Closed")
            elif show_passed:
                title = _("Passed")
            elif show_failed:
                title = _("Failed")
            elif show_tied:
                title = _("Tied")
            elif show_vetoed:
                title = _("Vetoed")

            body = """
<h2>%(title)s Polls (%(num_polls)d):</h2>
%(table_polls)s
""" %       {
                'title': title,
                'num_polls': len(table_shown_votes),
                'table_polls': table_template % {
                        'id': "table-polls",
                        'content': "\n".join(table_shown_votes),
                    },
            }

        return self.htmlOutline % {
                'title':        config.get("Voting", "weblist title") + " | "
                                    + ("Poll Overview" if public_view else "Poll Overview [restricted contents]"),
                'bgcolor':      config.get("Voting", "weblist background" if public_view else "weblist background restricted"),
                'fgcolor1':     config.get("Voting", "weblist color 1"),
                'fgcolor2':     config.get("Voting", "weblist color 2"),
                'textcolor':    config.get("Voting", "weblist text color"),
                'bordercolor':  config.get("Voting", "weblist border color"),
                'tablecolor':   config.get("Voting", "weblist table color"),
                'hovercolor':   config.get("Voting", "weblist hover color"),
                'urlcolor':     config.get("Voting", "weblist link color"),
                'webfont':      config.get("Voting", "weblist font"),
                'body':         body,
                'script':       "",
                'baseurl':      baseurl,
                'query':        query,
                #'script':       """
                #        $(document).ready(function() {
                #            $("#table-closed-polls").tablesorter({ sortList: [[0,1]] });
                #            $("#table-active-polls").tablesorter({ sortList: [[0,1]] });
                #        });
                #    """,
            }

    #------------------------------------------------------------------

    def print_poll_details(self, vote_id, baseurl="", query="", public_view=True):
        """Print details of given poll, including a list of all votes."""
        if not vote_id in self.votebot.voting.votes:
            return "Error: This poll does not exist!"
        v = self.votebot.voting.votes[vote_id]

        table_template = """
<table class="tablesorter" id="%(id)s">
    <thead>
        <tr>
            <th style="width:2em;">#</th>
            <th style="width:5em;">User</th>
            <th style="width:5em;">Vote</th>
            <th style="width:auto">Comment</th>
            <th style="width:10em;">Time</th>
        </tr>
    </thead>
    <tbody>
        %(content)s
    </tbody>
</table>
"""

        table_entry = """
<tr>
    <td>%(id)d.</td>
    <td>%(name)s</td>
    <td><b>%(result)s</b></td>
    <td class="title" title="%(comment)s">%(comment_html)s</td>
    <td title="%(time_dt)s">%(time)s</td>
</tr>
"""

        users = self.votebot.users.getUsers()
        admins = self.votebot.users.getAdmins()

        yesvotes, novotes = v.getResult()
        abstained = v.abstained
        leftvotes = 0
        numvotes  = yesvotes + novotes + abstained

        begin = v.getStart()
        end   = v.getEnd()
        title = cgi.escape(v.title)

        if v.status == Vote.VRUNNING:
            leftvotes = len(users) - numvotes
            status = '<span style="color:#CC0">running</span>'
            end = v.getTimeout()
        elif v.status == Vote.VCHALLENGED:
            leftvotes = len(admins) - numvotes
            status = '<span style="color:#C0C">challenged</span>'
            end = v.getTimeout()
        elif v.status == Vote.VSTOPPED:
            status = '<span style="color:#CCC">stopped</span>'
        elif v.status == Vote.VFAILED:
            status = '<span style="color:#C00">failed</span>'
        elif v.status == Vote.VPASSED:
            status = '<span style="color:#0C0">passed</span>'
        elif v.status == Vote.VTIED:
            status = '<span style="color:#00C">tied</span>'
        elif v.status == Vote.VVETOED:
            status = '<span style="color:#C80">vetoed</span>'
        elif v.status == Vote.VPOSTPONED:
            status = '<span style="color:#80C">postponed</span>'
        elif v.status == Vote.VDEPRECATED:
            status = '<span style="color:#C08">deprecated</span>'
        elif v.status == Vote.VDEPRECATED:
            status = '<span style="color:#0C8">decided</span>'
        else:
            status = "unknown"
        #status += " (#%d)" % v.status

        if not public_view or v.isClosed():
            table_votes = []
            for vote in v.results:
                if vote.result == VoteResult.VYES:
                    result = '<span style="color:#0C0">yes</span>'
                elif vote.result == VoteResult.VNO:
                    result = '<span style="color:#C00">no</span>'
                elif vote.result == VoteResult.VABSTAIN or vote.result == VoteResult.VAUTOABSTAIN:
                    result = '<span style="color:#00C">abstain</span>'
                elif vote.result == VoteResult.VVETO:
                    result = '<span style="color:#C80">veto</span>'
                else:
                    result = 'invalid'
                if vote.comment:
                    comment = vote.comment
                else:
                    comment = ""

                comment_markup = comment
                url = "{0}:{1}/poll/".format(config.get("Voting", "http server url"), config.get("Voting", "http server port"))
                comment_markup = _url_regex.sub(r'<a href="\1">\1</a>', comment_markup)
                comment_markup = _poll_regex.sub(r'\1<a href="{0}?{1}\3">\2</a>'.format(url, query), comment_markup)

                table_votes.append( table_entry % {
                        'id':           vote.index,
                        'name':         vote.name,
                        'result':       result,
                        'comment':      cgi.escape(comment, quote=True),
                        'comment_html': comment_markup,  # replace URLs with HTML links
                        'time':         _timestring(vote.create_dt),
                        'time_dt':      _timestring(vote.create_dt, useUTC=True),
                    })

            body = """
<h2>Poll #%(index)d: <b>%(title)s</b></h2>
<div class="info">
    <p><span class="field">Owner:</span> <b>%(creator)s</b></p>
    <p><span class="field">Begin:</span> <span title="%(begin_dt)s">%(begin)s</span></p>
    <p><span class="field">End:</span> <span title="%(end_dt)s">%(end)s</span></p>
    <p><span class="field">Status:</span> <b>%(status)s</b> (%(numvotes)d votes, %(leftvotes)d left)</p>
    <p><span class="field">Votes:</span> <span style="color:#0c0"><b>%(yesvotes)d</b></span> : <span style="color:#c00"><b>%(novotes)d</b></span> (<span style="color:#00c"><b>%(abstained)d</b></span> abstained)</p>
</div>

<h2>All Votes (%(numvotes)d):</h2>
%(table_votes)s
""" %               {
                        'index':        vote_id,
                        'title':        title,
                        'creator':      v.creator,
                        'begin':        _timestring(begin),
                        'end':          _timestring(end),
                        'begin_dt':     _timestring(begin, useUTC=True),
                        'end_dt':       _timestring(end, useUTC=True),
                        'status':       status,
                        'numvotes':     numvotes,
                        'leftvotes':    leftvotes,
                        'yesvotes':     yesvotes,
                        'novotes':      novotes,
                        'abstained':    abstained,
                        'table_votes':  table_template % {
                                'id': "table-votes",
                                'content': "\n".join(table_votes),
                        },
                    }
        else:
            body = """
<h2>Poll #%(index)d: <i>Not available in restricted mode!</i></h2>
"""  %              {
                        'index':        vote_id,
                    }

        return self.htmlOutline % {
                'title':        config.get("Voting", "weblist title") + " | "
                                    + ("Poll Details" if public_view else "Poll Details [restricted contents]"),
                'bgcolor':      config.get("Voting", "weblist background" if public_view else "weblist background restricted"),
                'fgcolor1':     config.get("Voting", "weblist color 1"),
                'fgcolor2':     config.get("Voting", "weblist color 2"),
                'textcolor':    config.get("Voting", "weblist text color"),
                'bordercolor':  config.get("Voting", "weblist border color"),
                'tablecolor':   config.get("Voting", "weblist table color"),
                'hovercolor':   config.get("Voting", "weblist hover color"),
                'urlcolor':     config.get("Voting", "weblist link color"),
                'webfont':      config.get("Voting", "weblist font"),
                'body':         body,
                'script':       "",
                'baseurl':      baseurl,
                'query':        query,
                #'script':       """
                #        $(document).ready(function() {
                #            $("#table-votes").tablesorter({ sortList: [[0,0]] });
                #        });
                #    """,
            }

    #------------------------------------------------------------------

    def print_users(self, baseurl="", query="", public_view=True):
        """Print list of all priviliged and normal users, using two tables."""
        all_users = sorted(self.votebot.users.users.values(), key=lambda x: x.index)

        table_template = """
<table class="tablesorter" id="%(id)s">
    <thead>
        <tr>
            <th style="width:2em;">#</th>
            <th style="width:5em;">User</th>
            <th style="width:auto;">Authname</th>
            <th style="width:10em;">Created</th>
            <th style="width:10em;">Last&nbsp;Activity</th>
        </tr>
    </thead>
    <tbody>
        %(content)s
    </tbody>
</table>
"""

        table_entry = """
<tr>
    <td>%(id)d.</td>
    <td><b>%(name)s</b></td>
    <td><i>%(authname)s</i></td>
    <td title="%(create_dt)s">%(created)s</td>
    <td title="%(inactive_dt)s">%(inactive)s</td>
</tr>
"""

        table_admins = []
        table_users = []
        for u in all_users:
            last_activity = self.votebot._get_last_activity(u.authname)
            if u.isOperator():
                table_admins.insert(0,table_entry % {
                        'id':           u.index,
                        'name':         u.name,
                        'authname':     "?" if public_view else u.authname,
                        'created':      "?" if public_view else _timestring(u.create_dt),
                        'inactive':     "?" if public_view else _timestring(last_activity),
                        'create_dt':    "" if public_view else _timestring(last_activity, useUTC=True),
                        'inactive_dt':  "" if public_view else _timestring(last_activity, useUTC=True),
                    })
            elif u.isValid():
                table_users.insert(0,table_entry % {
                        'id':           u.index,
                        'name':         u.name,
                        'authname':     "?" if public_view else u.authname,
                        'created':      "?" if public_view else _timestring(u.create_dt),
                        'inactive':     "?" if public_view else _timestring(last_activity),
                        'create_dt':    "" if public_view else _timestring(last_activity, useUTC=True),
                        'inactive_dt':  "" if public_view else _timestring(last_activity, useUTC=True),
                    })

        body =  """
<h2>Core Team (%(num_admins)d):</h2>
%(table_admins)s

<h2>Extended Team (%(num_users)d):</h2>
%(table_users)s
""" %       {
                'num_admins':   len(table_admins),
                'num_users':    len(table_users),
                'table_admins': table_template % {
                        'id': "table-admins",
                        'content': "\n".join(table_admins),
                    },
                'table_users':  table_template % {
                        'id': "table-users",
                        'content': "\n".join(table_users),
                    }
            }

        return self.htmlOutline % {
                'title':        config.get("Voting", "weblist title") + " | "
                                    + ("User Overview" if public_view else "User Overview [restricted contents]"),
                'bgcolor':      config.get("Voting", "weblist background" if public_view else "weblist background restricted"),
                'fgcolor1':     config.get("Voting", "weblist color 1"),
                'fgcolor2':     config.get("Voting", "weblist color 2"),
                'textcolor':    config.get("Voting", "weblist text color"),
                'bordercolor':  config.get("Voting", "weblist border color"),
                'tablecolor':   config.get("Voting", "weblist table color"),
                'hovercolor':   config.get("Voting", "weblist hover color"),
                'urlcolor':     config.get("Voting", "weblist link color"),
                'webfont':      config.get("Voting", "weblist font"),
                'body':         body,
                'script':       "",
                'baseurl':      baseurl,
                'query':        query,
                #'script':       """
                #        $(document).ready(function() {
                #            $("#table-users").tablesorter({ sortList: [[0,0]] });
                #            $("#table-admins").tablesorter({ sortList: [[0,0]] });
                #        });
                #    """,
            }

#######################################################################################################################

class VoteBot:
    """The main class representing the IRC voting interface."""

    def __init__(self, bot):
        """Plugin init

        Reads polls from config"""
        self.bot           = bot
        self.deferredWhois = {}
        self.deferredInfo  = {}
        self.authnames     = {}     # users in chatroom - nick : authname
        self.reminderShown = {}     # vote_id : flag
        self.lastReminders = {}     # vote_id : timestamp
        self.lastReminder  = None

        self.users = UserList(bot)

        def done(*args):
            log.msg("Loaded the following polls: {0}".\
                    format(", ".join([ str(idx) for idx in self.voting.votes.keys() ])))
                    #format([ "{0} ({1} votes)".format(k, len(self.voting.votes[k].results)) for k in self.voting.votes.keys() ]))
        self.voting = VotingSystem(self, cback=done)

    def _cleanup(self):
        """cleanup hook is called when the bot leaves its home channel."""
        return self.httpServer.handle.stopListening();

    def _postInit(self):
        ## TODO - re-check if this is true
        """post-init hook is called when the bot joins its home channel. (?)"""
        try:
            self.httpServer.handle.startListening();
        except:
            self.httpServer = VoteHTTPServer(self)
        self.timeout_thread = thread.start_new_thread(self._timeout_loop, ())


    #------------------------------------------------------------------

    def _split_reply(self, user, msg, delim="|", minlength=50, delay=0.2):
        """split given reply message, used for long replies."""
        if len(msg) > minlength:
            for m in msg.split(delim):
                self.bot.pmsg(user, m)
                sleep(delay)
        else:
            self.bot.pmsg(user, ", ".join(msg.split(delim)))

    def _split_message(self, channel, msg, delim="|", minlength=50, delay=0.2):
        """split given channel message, used for long messages."""
        if len(msg) > minlength:
            for m in msg.split(delim):
                self.bot.cmsg(channel, m)
                sleep(delay)
        else:
            self.bot.cmsg(channel, ", ".join(msg.split(delim)))

    #------------------------------------------------------------------

    def _get_active_vote(self, force=False):
        """Return the currently active poll.

        The active poll is the LATEST one which is currently running and active.
        If "force" is set, only the latest poll is returned, regardless of its status.
        """
        if force:
            return self.voting.votes.values()[-1]  # last item
        else:
            curtime = itime()
            if len(self.voting.votes) > 0:
                for vote in reversed(self.voting.votes.values()):  # last vote comes first
                    if vote.isRunning() and vote.isActive(curtime):
                        return vote
        return None

    #------------------------------------------------------------------

    def _get_vote(self, ident, title, active=True):
        """Return a specific, active poll."""
        curtime = itime()
        if len(self.voting.votes) > 0:
            for vote in reversed(self.voting.votes.values()):  # last vote comes first
                if vote.title == title and vote.creator == ident:
                    if active:
                        if vote.isRunning() and vote.isActive(curtime):
                            return vote
                    else:
                        return vote
        return None

    #------------------------------------------------------------------

    def _create_vote(self, user, title, status=Vote.VRUNNING):
        timeout = _convert_time(config.get("Voting", "default timeout").encode("string-escape"))
        return self.voting._insert_vote(title, timeout, user, status)

    #def _remove_vote(self, index):
    #    return self.voting._delete(index)

    def _update_vote(self, index, timeout, end_dt, status, result):
        return self.voting._update_vote(index, timeout, end_dt, status, result)

    def _rename_vote(self, index, title):
        return self.voting._rename_vote(index, title)

    #------------------------------------------------------------------

    def _add_result(self, vote_id, user, result, comment=""):
        if type(user) == list:
            return self.voting._insert_result_multi(vote_id, user, result, comment)
        else:
            return self.voting._insert_result(vote_id, user, result, comment)

    def _update_result(self, vote_id, user, result, comment=""):
        return self.voting._update_result(vote_id, user, result, comment)

    #------------------------------------------------------------------

    def _add_user(self, nick, ident, status=User.UNORMAL):
        return self.users._add_user(nick, ident, status)

    #def _del_user(self, ident):
    #    return self.users._del_user(ident)

    def _mod_user(self, ident, status=User.UNORMAL):
        return self.users._modify_user(ident, status)

    #------------------------------------------------------------------

    def _timeout_loop(self):
        """Timeout loop called in a separate thread, checks timeouts of all active polls."""
        log.msg("Starting timeout-check loop")
        check_interval = float(config.get('Voting', 'timeout check interval'))
        while True:
            self._check_timeout()
            sleep( check_interval )

    #------------------------------------------------------------------

    def _check_timeout(self):
        """Check timeouts of all polls, and update if a poll is overude."""
        # Make sure that only players who have added up are checked
        curtime = itime()

        activevotes = {}
        for k,v in self.voting.votes.items():
            if v.isRunning():
                activevotes[k] = v

        if len(activevotes) == 0:
            return

        log.msg("Checking timeout for {0} polls: {1}".format(len(activevotes), [v.index for v in activevotes.values()]))

        timedout_votes = []
        pretimeout = _convert_time(config.get("Voting", "pre-timeout window").encode("string-escape"))
        reminder_interval = _convert_time(config.get("Voting", "vote reminder").encode("string-escape"))
        for k,vote in activevotes.items():
            if vote.isOverdue(curtime):
                log.msg("Poll #{0} {1} is overdue!".format(vote.index, vote))
                if vote.getStatus() == Vote.VRUNNING:
                    numusers = len(self.users.getUsers())
                elif vote.getStatus() == Vote.VCHALLENGED:
                    numusers = len(self.users.getAdmins())
                else:
                    continue

                def done(result, vote):
                    cyes, cno = vote.getResult()
                    cabstain = vote.getAbstained()
                    # TODO - simple majority rule, probably needs to change
                    status = Vote.VERROR
                    if cyes > cno:
                        status = Vote.VPASSED
                    elif cyes < cno:
                        status = Vote.VFAILED
                    else:
                        status = Vote.VTIED
                    leftusers = numusers - (cyes + cno + cabstain)

                    def done(result, vote):
                        self.bot.fire('vote_timedout', vote.index, leftusers, curtime)
                        timedout_votes.append(vote.index)
                    self._update_vote(vote.index, timeout=vote.timeout, end_dt=curtime, status=status, result=vote.result).\
                            addCallback(done, vote)
                #vote._getResults().addCallback(done, vote)
                done(True, vote)
            elif vote.isRunningout(pretimeout, curtime):
                self.bot.fire('vote_runningout', vote.index, curtime)
            #elif vote.index in self.lastReminders:
            #    if curtime - self.lastReminders[vote.index] > reminder_interval:
            #        self.bot.fire('vote_reminder', vote.index, curtime)

        votes_left = len(activevotes) - len(timedout_votes)
        if votes_left > 0:
            if not self.lastReminder or (curtime - self.lastReminder > reminder_interval):
                self.bot.fire('global_reminder', votes_left, curtime)

    #------------------------------------------------------------------

    def _check_vote_status(self, vote):
        """Check status of given poll, and update if a majority of votes has been reached."""
        curtime = itime()
        if vote.getStatus() == Vote.VRUNNING:
            numusers = len(self.users.getUsers())
        elif vote.getStatus() == Vote.VCHALLENGED:
            numusers = len(self.users.getAdmins())
        else:
            return

        def done(*args):
            cyes, cno = vote.getResult()
            cabstain = vote.getAbstained()
            numusers = args[1]
            leftusers = numusers - (cyes + cno + cabstain)

            # TODO - simple majority rule, probably needs to change
            majority = int(ceil(numusers / 2))
            if cyes > majority and cno < majority:
                def done(*args):
                    self.bot.fire('vote_passed', vote.index, leftusers)
                self._update_vote(vote.index, timeout=vote.timeout, end_dt=curtime, status=Vote.VPASSED, result=(cyes,cno)).addCallback(done)
            elif cno > majority and cyes < majority:
                def done(*args):
                    self.bot.fire('vote_failed', vote.index, leftusers)
                self._update_vote(vote.index, timeout=vote.timeout, end_dt=curtime, status=Vote.VFAILED, result=(cyes,cno)).addCallback(done)
            elif cyes == majority and cno == majority:
                def done(*args):
                    self.bot.fire('vote_tied', vote.index, leftusers)
                self._update_vote(vote.index, timeout=vote.timeout, end_dt=curtime, status=Vote.VTIED, result=(cyes,cno)).addCallback(done)
        #vote._getResults().addCallback(done, numusers)
        done(True, numusers)

    #------------------------------------------------------------------

    def _whois(self, user):
        if not user in self.deferredWhois:
            self.deferredWhois[user] = []
        log.msg("whois'ing user {0}".format(user))
        d = defer.Deferred()
        self.deferredWhois[user].append(d)
        self.bot.sendLine("WHOIS %s" % (user))

        reactor.callLater(10, self._whoisTimeout, user)  # timeout after 10 seconds
        return d

    def _whoisTimeout(self, user):
        if user in self.deferredWhois:
            if len(self.deferredWhois[user]) > 0:
                log.msg("whois timeout for user {0}".format(user))
                self.deferredWhois[user][0].callback(False)  # use first (oldest) item
                del self.deferredWhois[user][0]

    #------------------------------------------------------------------

    def _nickservInfo(self, user):
        if user in self.deferredInfo:
            self._nickservInfoTimeout(user)
        self.bot.sendLine("MSG NickServ INFO %s" % (user))
        d = defer.Deferred()
        self.deferredInfo[user] = d

        reactor.callLater(10, self._nickservInfoTimeout, user)  # timeout after 10 seconds
        return d

    def _nickservInfoTimeout(self, user):
        if user in self.deferredInfo:
            try:
                self.deferredInfo[user].callback(False)
            except:
                pass
            del self.deferredInfo[user]

    #------------------------------------------------------------------

    def _get_authname(self, user):
        if user in self.authnames:
            return self.authnames[user]

        def done(result):
            if result and user in self.authnames:
                return self.authnames[user]
            else:
                return None
        self._whois(user).addCallback(done)

    #------------------------------------------------------------------

    def _get_last_activity(self, authname):
        last_active = 0
        for k,v in self.voting.votes.items():
            votes = v.getVotes()
            if authname in votes.keys():
                if votes[authname].result == VoteResult.VAUTOABSTAIN:
                    # exclude auto-abstains of course!
                    continue
                create_dt = votes[authname].create_dt
                if create_dt > last_active:
                    last_active = create_dt
        #log.msg("Last activity for {0} on {1} (now = {2})".format(authname, last_active, itime()))
        return last_active

    #------------------------------------------------------------------

    def _get_running_votes(self):
        activevotes = []
        curtime = itime()
        for v in self.voting.votes.values():
            if v.isActive(curtime):
                activevotes.append(v)
        log.msg("Running votes at {0}: {1}".format(_timestring(curtime, useUTC=True), activevotes))
        return activevotes

    def _get_missing_votes(self, authname):
        activevotes = []
        for v in self.voting.votes.values():
            if not authname in v.getVotes():
                if v.getStatus() == Vote.VRUNNING:
                    if authname in self.users.getUsers():
                        activevotes.append(v)
                elif v.getStatus() == Vote.VCHALLENGED:
                    if authname in self.users.getAdmins():
                        activevotes.append(v)
        log.msg("Missing votes for {0}: {1}".format(authname, activevotes))
        return activevotes

    #------------------------------------------------------------------

    def _post_tracker_item(self, vote_id):
        vote = self.voting[vote_id]

        if vote.getStatus() != Vote.VPASSED:
            log.msg("Poll #{0} has not passed, won't create any tracker item!".format(vote_id))
            return

        log.msg("Posting tracker item for poll #{0} on {1}".format(vote_id, config.get("Voting", "redmine server url").decode('string-escape')))

        def done(*args):
            prefix = config.get("Voting", "redmine subject prefix").decode('string-escape')
            server_port = config.getint("Voting", "redmine server port")
            server_url = config.get("Voting", "redmine server url").decode('string-escape')
            tracker_path = config.get("Voting", "redmine tracker path").decode('string-escape')
            accesskey = config.get("Voting", "redmine access key").decode('string-escape')
            project_id = config.getint("Voting", "redmine project id")
            tracker_id = config.getint("Voting", "redmine tracker id")

            yesvotes, novotes = vote.getResult()
            abstained = vote.getAbstained()

            description = """
h2. Poll {0} by {1}

bq. {6}

*The poll passed with {2}:{3} majority. {4} users abstained the poll.*

_For details see:  {5}_


h2. List of votes

|_. Name |_. Vote |_. Comment |
""".\
            format(vote.index, vote.creator, yesvotes, novotes, abstained, vote._poll_url(no_markup=True, no_tinyurl=True), vote.title)

            votes = vote.getVotes()
            for name in sorted(votes.keys()):
                v = votes[name]
                description += "| {0} | {1} | {2} |\n".format(v.name, v.getResult(), v.comment)

            payload = {
                "issue": {
                        "subject":      "{0} #{1} - {2} (by {3})".format(prefix, vote.index, vote._short_title(12), vote.creator),
                        "project_id":   project_id,     # Tracker
                        "tracker_id":   tracker_id,      # Feature
                        "description":  description,
                },
                "key":  accesskey,
            }
            headers = {
                "content-type":         "application/json",
                "X-Redmine-API-Key":    accesskey,
            }

            #print payload
            try:
                conn = httplib.HTTPConnection(server_url, server_port, timeout=10)
                conn.request("POST", tracker_path+".json", json.dumps(payload), headers)
                response = conn.getresponse()
                log.msg("Posted new issue on redmine: {0} {1}".format(response.status, response.reason));
            except:
                log.msg("Failed to post new issue on redmine: {0} {1}".format(response.status, response.reason));
        #vote._getResults().addCallback(done)
        done(True)

    def _send_notification_mail(self, vote_id, subject, is_new=False):
        vote = self.voting[vote_id]

        def done(*args):
            users = self.users.getUsers()
            admins = self.users.getAdmins()
            yesvotes, novotes = vote.getResult()
            abstained = vote.abstained
            leftvotes = 0
            numvotes = yesvotes + novotes + abstained
            begin = _timestring(vote.getStart(), useUTC=True)
            end = _timestring(vote.getEnd(), useUTC=True)

            status = ""
            if vote.status == Vote.VRUNNING:
                leftvotes = len(users) - numvotes
                end = _timestring(vote.getTimeout(), useUTC=True)
                status = "running"
            elif vote.status == Vote.VCHALLENGED:
                leftvotes = len(admins) - numvotes
                end = _timestring(vote.getTimeout(), useUTC=True)
                status = "challenged"
            elif vote.status == Vote.VSTOPPED:
                status = "stopped"
            elif vote.status == Vote.VFAILED:
                status = "failed"
            elif vote.status == Vote.VPASSED:
                status = "passed"
            elif vote.status == Vote.VTIED:
                status = "tied"
            elif vote.status == Vote.VVETOED:
                status = "vetoed"
            elif vote.status == Vote.VPOSTPONED:
                status = "postponed"
            elif vote.status == Vote.VDEPRECATED:
                status = "deprecated"
            elif vote.status == Vote.VDECIDED:
                status = "decided"
            else:
                status = "unknown"

            votelist = []
            votes = vote.getVotes()
            for name in sorted(votes.keys()):
                votelist.append("  * {0}".format(repr(votes[name])))

            if bool(config.get("Voting", "automail")):
                prefix = config.get("Voting", "automail subject prefix").decode('string-escape')
                from_address = config.get("Voting", "automail from address").decode('string-escape')
                to_addresses = config.get("Voting", "automail to address").decode('string-escape').split(" ")

                msg = MIMEText("""
Info on poll #%(id)d:
Title: %(title)s
Owner: %(creator)s
Begin: %(begin)s
End: %(end)s
Status: %(status)s (%(numvotes)d votes, %(leftvotes)d left)
Votes: %(yesvotes)d : %(novotes)d (%(abstained)d abstained)

Vote list:
%(votelist)s

Poll details available at: %(url)s
""" %               {
                        'id': vote_id,
                        'title': vote.title,
                        'creator': vote.creator,
                        'begin': begin,
                        'end': end,
                        'status': status,
                        'numvotes': numvotes,
                        'leftvotes': leftvotes,
                        'yesvotes': yesvotes,
                        'novotes': novotes,
                        'abstained': abstained,
                        'votelist': "\n".join(votelist),
                        'url': "{0}:{1}/poll/{2}".format(
                                    config.get("Voting", "http server url"),
                                    config.get("Voting", "http server port"),
                                    vote_id ),
                    })

                if is_new:
                    msg['Subject'] = "{0} New poll #{1} by {2} {3}".format(prefix, vote_id, vote.creator, subject)
                else:
                    msg['Subject'] = "{0} Poll #{1} by {2} {3}".format(prefix, vote_id, vote.creator, subject)

                try:
                    s = smtplib.SMTP('localhost')
                    for to_address in to_addresses:
                        msg['From'] = from_address
                        msg['To'] = to_address
                        log.msg("Sending notification mail to: {0}".format(to_address))
                        s.sendmail(from_address, [to_address], msg.as_string())
                    s.quit()
                except:
                    log.msg("Failed to send notification mail (error: {0})".format(sys.exc_info()[0]))

        #vote._getResults().addCallback(done)
        done(True)

    #------------------------------------------------------------------

    def _check_invite(self):
        d = FetchedList.get_users(self.bot, self.bot.channel).get()
        def _fireEvent(userlist):
            def done(*args):
                #print args
                return
            users = []
            for ident in self.users.users:
                users.append(ident)
            log.msg("Allowed users in {0}: {1}".format(self.bot.channel, users))
            for nick, ident, host, flags in userlist:
                #print nick, ident
                ident = ident.split("~")[1]
                if ident in users:
                    users.remove(ident)
            log.msg("Unjoined users in {0}: {1}".format(self.bot.channel, users))

            for ident in users:
                self._nickservInfo(ident).addCallback(done)
        d.addCallback(_fireEvent)

    #------------------------------------------------------------------

    def _auto_abstain_users(self, vote_id, curtime=None):
        if not curtime:
            curtime = itime()
        vote = self.voting[vote_id]

        if vote.status == Vote.VRUNNING:
            all_users = self.users.getUsers().keys()
        elif vote.status == Vote.VCHALLENGED:
            all_users = self.users.getAdmins().keys()
        else:
            return

        inactive_users = []
        inactivity_delta = _convert_time(config.get("Voting", "inactivity period to auto-abstain").encode("string-escape"))
        for authname in all_users:
            if curtime - self._get_last_activity(authname) > inactivity_delta:
                inactive_users.append(authname)
        log.msg("Found {1} inactive users (inactivity period {0}): {2}".format(inactivity_delta, len(inactive_users), inactive_users))

        votes = vote.getVotes()
        for ident in inactive_users:
            if ident in votes.keys():
                inactive_users.remove(ident)

        if len(inactive_users) == 0:
            return

        def done(*args):
            self.bot.fire('vote_auto_abstained', vote_id, inactive_users)
        self._add_result(vote.index, inactive_users, VoteResult.VAUTOABSTAIN, _("(auto-abstain due to inactivity)")).addCallback(done)

    #==================================================================

    def isAllowed(self, nick):
        if nick in self.authnames:
            ident = self.authnames[nick]
            if ident in self.users.getUsers().keys():
                return True
        return False

    def isAllowedIdent(self, ident):
        if ident in self.users.getUsers().keys():
            return True
        return False

    def isAdmin(self, nick):
        if nick in self.authnames:
            ident = self.authnames[nick]
            if ident in self.users.getAdmins().keys():
                return True
        return False

    def isKnownAuthname(self, ident):
        if ident in self.authnames.values():
            return True
        return False

    def messageAll(self, msg, only_admins=False):
        if not int(config.get("Voting", "auto highlight")):
            return

        if only_admins:
            users = self.users.getAdmins().values()
        else:
            users = self.users.getUsers().values()
        for user in users:
            self.bot.pmsg(user.name, msg)
            sleep(0.2)

    def messageOne(self, ident, msg):
        if not int(config.get("Voting", "auto highlight")):
            return

        users = self.users.getUsers().values()
        for user in users:
            if user.authname == ident:
                self.bot.pmsg(user.name, msg)
            sleep(0.2)

    #==================================================================

    def callvote(self, call, args):
        """!callvote [-YES|-NO|-ABSTAIN] <topic>

        Call a new poll with given topic, using the default timeout."""
        flag = None

        vote = self._get_active_vote()
        #if vote:
        #    call.reply(_("Sorry, only one poll can run at the same time!"))
        #    return

        if len(args) > 0:
            if args[0][0] == "-":
                flag = args[0][1:].upper()
                del args[0]

        if len(args) < 1:
            call.reply(_("Sorry, you have to specify some title for the new poll!"))
            return

        try:
            title = u" ".join(args)
            if not all(c in _allowed_chars for c in title):
                raise InputError
        except:
            call.reply(_("Sorry, please only use valid characters in the title!"))
            return

        def user_check(success):
            if not success:
                call.reply(_("Sorry, only registered accounts can vote!"))
                return

            if not self.isAllowed(call.nick):
                call.reply(_("Sorry, you have not been permitted to vote!"))
                return

            ident = self.authnames[call.nick]
            def cback(*args):
                def done(*args):
                    def done(*args):
                        self.bot.fire('vote_started', vote.index, call.nick)
                    vote = self._get_vote(ident=ident, title=title)  # get new vote
                    if vote:
                        if flag == "YES":
                            result = VoteResult.VYES
                        elif flag == "NO":
                            result = VoteResult.VNO
                        elif flag == "ABSTAIN":
                            result = VoteResult.VABSTAIN
                        else:
                            result = VoteResult.VYES
                        self._add_result(vote.index, ident, result).addCallback(done)
                    else:
                        call.reply(_("Some error occured!"))
                        return
                self.voting.reload(done)
            self._create_vote(ident, title).addCallback(cback)
        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def decide(self, call, args):
        """!decide <topic>

        Decide on the given subject (only available to admins). This is essentially a poll without voting.
        """
        flag = None

        vote = self._get_active_vote()
        #if vote:
        #    call.reply(_("Sorry, only one poll can run at the same time!"))
        #    return

        #if len(args) > 0:
        #    if args[0][0] == "-":
        #        flag = args[0][1:].upper()
        #        del args[0]

        if len(args) < 1:
            call.reply(_("Sorry, you have to specify some title for the new poll!"))
            return

        try:
            title = u" ".join(args)
            if not all(c in _allowed_chars for c in title):
                raise InputError
        except:
            call.reply(_("Sorry, please only use valid characters in the title!"))
            return

        def user_check(success):
            if not success:
                call.reply(_("Sorry, only registered accounts can vote!"))
                return

            if not self.isAdmin(call.nick):
                call.reply(_("Sorry, you have not been permitted to vote!"))
                return

            ident = self.authnames[call.nick]
            def cback(*args):
                def done(*args):
                    def done(*args):
                        self.bot.fire('vote_decided', vote.index, call.nick)
                    vote = self._get_vote(ident=ident, title=title)  # get new vote
                    if vote:
                        self._update_vote(vote.index, timeout=vote.timeout, end_dt=itime(), status=Vote.VDECIDED, result=vote.result).addCallback(done)
                    else:
                        call.reply(_("Some error occured!"))
                        return
                self.voting.reload(done)
            self._create_vote(ident, title, status=Vote.VRUNNING).addCallback(cback)
        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def stopvote(self, call, args):
        """"!vstop <id>

        Stop the given poll (only allowed to the poll creator)."""
        vote = None

        if len(args) < 1:
            call.reply(_("You have to specify the id of the poll you want to stop!"))
            return

        if len(args) > 0:
            vote_id = int(args[0][1:] if args[0].startswith("#") else args[0])
            try:
                vote = self.voting.votes[vote_id]
            except:
                call.reply(_("Sorry, poll #{0} is not available!").format(vote_id))
                return
        #else:
        #    vote = self._get_active_vote()
        #    if not vote:
        #        call.reply(_("Sorry, there is no poll running right now!"))
        #        return

        if not vote.isActive():
            call.reply(_("Sorry, poll #{0} is no longer active!").format(vote_id))
            return

        if not vote.getStatus() in [ Vote.VRUNNING, Vote.VPOSTPONED, Vote.VCHALLENGED, Vote.VDECIDED ]:
            call.reply(_("Sorry, poll #{0} appears to be inactive right now!").format(vote_id))
            return

        def user_check(success):
            if not success:
                call.reply(_("Sorry, only registered accounts can vote!"))
                return

            # TODO - allow admins to stop votes? - no! -> but veto is allowed

            if self.authnames[call.nick] != vote.creator:
                call.reply(_("Sorry, only the poll creator can stop this poll!"))
                return

            if not self.isAllowed(call.nick):
                # this should not happen, but check anyway
                call.reply(_("Sorry, you have not been permitted to vote!"))
                return

            d = call.confirm("Do you really want to \x02stop\x02 the poll titled \x02\x0306{0}\x0f?".format(vote.title))
            def _confirmed(ret):
                if ret:
                    def done(*args):
                        self.bot.fire('vote_stopped', vote.index, call.nick)
                    self._update_vote(vote.index, timeout=vote.timeout, end_dt=itime(), status=Vote.VSTOPPED, result=vote.result).addCallback(done)
                else:
                    call.reply(_("Cancelled."))
            return d.addCallback(_confirmed)
        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def postpone(self, call, args):
        """!postpone [id]

        Postpone the given poll (only available to admins)."""
        vote = None

        if len(args) < 1:
            call.reply(_("You have to specify the id of the poll you want to postpone!"))
            return

        if len(args) > 0:
            vote_id = int(args[0][1:] if args[0].startswith("#") else args[0])
            try:
                vote = self.voting.votes[vote_id]
            except:
                call.reply(_("Sorry, poll #{0} is not available!").format(vote_id))
                return
        #else:
        #    vote = self._get_active_vote()
        #    if not vote:
        #        call.reply(_("Sorry, there is no poll running right now!"))
        #        return

        if not vote.isActive():
            call.reply(_("Sorry, poll #{0} is no longer active!").format(vote_id))
            return

        if vote.getStatus() != Vote.VRUNNING:
            call.reply(_("Sorry, poll #{0} appears to be inactive!").format(vote_id))
            return

        def user_check(success):
            if not success:
                call.reply(_("Sorry, only registered accounts can vote!"))
                return

            if not self.isAdmin(call.nick):
                call.reply(_("Sorry, only privileged users can postpone votes!"))
                return

            d = call.confirm("Do you really want to \x02postpone\x02 the poll titled \x02\x0306{0}\x0f?".format(vote.title))
            def _confirmed(ret):
                if ret:
                    def done(*args):
                        self.bot.fire('vote_postponed', vote.index, call.nick)
                    self._update_vote(vote.index, timeout=vote.timeout, end_dt=itime(), status=Vote.VPOSTPONED, result=vote.result).addCallback(done)
                else:
                    call.reply(_("Cancelled."))
            return d.addCallback(_confirmed)
        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def resume(self, call, args):
        """!resume <id>

       Resume the given postponed poll (only available to admins)."""
        vote = None

        if len(args) < 1:
            call.reply(_("You have to specify the id of the poll you want to resume!"))
            return

        if len(args) > 0:
            vote_id = int(args[0][1:] if args[0].startswith("#") else args[0])
            try:
                vote = self.voting.votes[vote_id]
            except:
                call.reply(_("Sorry, poll #{0} is not available!").format(vote_id))
                return
        #else:
        #    vote = self._get_active_vote()
        #    if not vote:
        #        call.reply(_("Sorry, there is no poll running right now!"))
        #        return

        #if not vote.isActive():
        #    call.reply(_("Sorry, poll #{0} is no longer active!").format(vote_id))
        #    return

        if vote.getStatus() != Vote.VPOSTPONED:
            call.reply(_("Sorry, poll #{0} appears not to be postponed right now!").format(vote_id))
            return

        def user_check(success):
            if not success:
                call.reply(_("Sorry, only registered accounts can vote!"))
                return

            if not self.isAdmin(call.nick):
                call.reply(_("Sorry, only privileged users can postpone votes!"))
                return

            d = call.confirm("Do you really want to \02resume\x02 the poll titled \x02\x0306{0}\x0f?".format(vote.title))
            def _confirmed(ret):
                if ret:
                    def done(*args):
                        self.bot.fire('vote_resumed', vote.index, call.nick)
                    timeout = itime() + vote.timeout - (vote.end_dt - vote.create_dt) # extend timeout
                    self._update_vote(vote.index, timeout=timeout, end_dt=0, status=Vote.VRUNNING, result=vote.result).addCallback(done)
                else:
                    call.reply(_("Cancelled."))
            return d.addCallback(_confirmed)
        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def result(self, call, args):
        """!results [id]

        Show info on the given vote, or the currently running one if no id is given."""
        if len(args) > 0:
            vote_id = int(args[0][1:] if args[0].startswith("#") else args[0])
            try:
                vote = self.voting.votes[vote_id]
                call.reply("#{0}: {1}".format(vote.index, str(vote)))
            except:
                call.reply(_("Sorry, poll #{0} does not exist!").format(vote_id))
        else:
            vote = self._get_active_vote()
            if vote:
                call.reply(_("Latest poll is #{0}: {1}").format(vote.index, str(vote)))
            else:
                call.reply(_("Sorry, there is no poll running right now!"))

    #------------------------------------------------------------------

    def details(self, call, args):
        """!details <id>

        Show details on the given poll."""
        vote = None

        if len(args) < 1:
            call.reply(_("You have to specify the id of the poll you want to vote on!"))
            return

        if len(args) > 0:
            vote_id = int(args[0][1:] if args[0].startswith("#") else args[0])
            try:
                vote = self.voting.votes[vote_id]
            except:
                call.reply(_("Sorry, poll #{0} does not exist!").format(vote_id))
                return
        #else:
        #    vote = self._get_active_vote()
        #    if not vote:
        #        call.reply(_("Sorry, there is no poll running right now!"))
        #        return

        reply = "Poll #{0}: {1}".format(vote.index, str(vote))

        def done(*args):
            autoabstained = []
            voteresults = []
            for r in vote.results:
                if r.result == VoteResult.VAUTOABSTAIN:
                    autoabstained.append(r.name)
                else:
                    voteresults.append(r)
            call.reply(args[1] + _("\x20Votes:"))
            self._split_reply(call.nick, "|".join([ str(r) for r in voteresults ]))
            call.reply(_("\x02\x0315Auto-abstained:\x0f {0}").format(", ".join(sorted(autoabstained))))
        #vote._getResults().addCallback(done, reply)
        done(True, reply)

    #------------------------------------------------------------------

    def votes(self, call, args):
        """!polls [-ALL|-OWN|-OPEN] [timerange]

        Show summary of running polls. With -ALL, show all existing polls;
        with -OWN, show polls created by you; with -OPEN, show polls
        on which you haven't voted yet. Specify a timerange (e.g. "2 weeks")
        to only list polls created after that time.
        """
        curtime = itime()
        show_all = False
        show_own = False
        show_unvoted = False
        flag = None
        timerange = None

        if len(args) > 0:
            if args[0][0] == "-":
                flag = args[0][1:].upper()
                del args[0]

        if len(args) > 0:
            try:
                timerange = _convert_time(" ".join(args[0:]))
            except:
                timerange = None

        if flag == "ALL":
            show_all = True
        elif flag == "OWN":
            show_own = True
        elif flag == "OPEN":
            show_unvoted = True

        def user_check(success):
            if not success:
                call.reply(_("Sorry, only registered accounts can vote!"))
                return

            ident = self.authnames[call.nick]

            missingvotes = self._get_missing_votes(ident)
            index_missing = [ v.index for v in missingvotes ]

            activevotes = []
            for vote in self.voting.votes.values():
                if timerange:
                    if vote.getStart() < curtime-timerange:
                        continue
                if show_own:
                    if vote.getCreator() == ident:
                        activevotes.append(vote)
                    continue
                if vote.isActive(curtime) and show_unvoted:
                    if vote.index in index_missing:
                        activevotes.append(vote)
                    continue
                if vote.isActive(curtime) or show_all:
                    activevotes.append(vote)

            if len(activevotes) > 0:
                if len(activevotes) == 1:
                    reply = _("There is 1 poll")
                else:
                    reply = _("There are {0} polls").format(len(activevotes))

                if show_all:
                    reply += _(" in total:\x0f")
                elif show_own:
                    reply += _(" owned by you:\x0f")
                elif show_unvoted:
                    reply += _(" on which you haven't voted yet:\x0f")
                else:
                    reply += _(" running right now:\x0f")

                votelist = []
                for v in activevotes:
                    if ident in v.getVotes().keys():
                        result = v.getVotes()[ident].result
                        if result == VoteResult.VYES:
                            color = "\x0303"
                        elif result == VoteResult.VNO:
                            color = "\x0304"
                        elif result == VoteResult.VABSTAIN or result == VoteResult.VAUTOABSTAIN:
                            color = "\x0302"
                        else:
                            color = "\x0305"
                    else:
                        color = "\x0306"
                    votelist.append("#{0}. \x02{2}{1}\x0f".format(v.index, v.title, color))

                call.reply(reply)
                self._split_reply(call.nick, "|".join(votelist))

            else:
                if show_all:
                    call.reply(_("There are no polls yet."))
                elif show_own:
                    call.reply(_("There are no polls owned by you."))
                elif show_unvoted:
                    call.reply(_("There are no polls on which you haven't voted yet."))
                else:
                    call.reply(_("There are no polls running right now."))

        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def vote(self, call, args):
        """!vote {yes|no|abstain} <id> [comment]

        Vote on the currently running poll."""
        vote = None
        result = None
        comment = ""

        if len(args) < 1:
            call.reply(_("Sorry, but you have to specify a vote!"))
            return

        if len(args) < 2:
            call.reply(_("You have to specify the id of the poll you want to vote on!"))
            return

        if len(args) > 1:
            vote_id = int(args[1][1:] if args[1].startswith("#") else args[1])
            try:
                vote = self.voting.votes[vote_id]
            except:
                call.reply(_("Sorry, poll #{0} is not available!").format(vote_id))
                return
        #else:
        #    vote = self._get_active_vote()
        #    if not vote:
        #        call.reply(_("Sorry, there is no poll running right now!"))
        #        return

        if not vote.isActive():
            call.reply(_("Sorry, poll #{0} is no longer active!").format(vote_id))
            return

        votum = args[0].lower()
        if votum == "abstain":
            result = VoteResult.VABSTAIN
        elif votum == "yes":
            result = VoteResult.VYES
        elif votum == "no":
            result = VoteResult.VNO
        else:
            call.reply(_("Sorry, you can only vote 'yes', 'no' or 'abstain'!"))
            return

        if len(args) >= 3:
            comment = " ".join(args[2:])

        def user_check(success):
            if not success:
                call.reply(_("Sorry, only registered accounts can vote!"))
                return

            def done(*args):
                if vote.getStatus() == Vote.VRUNNING:
                    if not self.isAllowed(call.nick):
                        call.reply(_("Sorry, you have not been permitted to vote!"))
                        return
                elif vote.getStatus() == Vote.VCHALLENGED:
                    if not self.isAdmin(call.nick):
                        call.reply(_("Sorry, you have not been permitted to vote!"))
                        return
                else:
                    call.reply(_("Sorry, poll #{0} is no longer active!").format(vote_id))
                    return

                ident = self.authnames[call.nick]
                votes = vote.getVotes()
                if ident in votes.keys():
                    #if votes[ident].result == result:
                    #    call.reply(_("Sorry, but you have voted \x02{0}\x02 already!").format(votum))
                    #    return
                    if votes[ident].result == VoteResult.VABSTAIN:
                        oldvotum = "abstain"
                    elif votes[ident].result == VoteResult.VYES:
                        oldvotum = "yes"
                    elif votes[ident].result == VoteResult.VNO:
                        oldvotum = "no"
                    elif votes[ident].result == VoteResult.VVETO:
                        # this should not happen?
                        oldvotum = "veto"
                    elif votes[ident].result == VoteResult.VAUTOABSTAIN:
                        oldvotum = "abstain"
                    else:
                        oldvotum = ""

                    if oldvotum:
                        if votes[ident].result == VoteResult.VAUTOABSTAIN:
                            self.bot.pmsg(call.nick, _("Note: You were auto-abstained on this poll due to inactivity."))
                        if oldvotum == votum:
                            msg = _("Do you want to update your \x02{1}\x02 vote on the poll titled \x02\x0306{0}\x0f?").\
                                    format(vote.title, oldvotum)
                        else:
                            msg = _("Do you want to change your vote from \x02{1}\x02 to \x02{2}\x02 on the poll titled \x02\x0306{0}\x0f?").\
                                    format(vote.title, oldvotum, votum)
                        d = call.confirm("You have already voted! " + msg)
                        def _confirmed(ret):
                            if ret:
                                def done(*args):
                                    self.bot.fire('votum_changed', vote.index, call.nick, oldvotum, votum, comment)
                                    self._check_vote_status(vote)
                                self._update_result(vote.index, ident, result, comment).addCallback(done)
                            else:
                                call.reply(_("Cancelled."))
                        d.addCallback(_confirmed)
                    else:
                        def done(*args):
                            self.bot.fire('votum_changed', vote.index, call.nick, oldvotum, votum, comment)
                            self._check_vote_status(vote)
                        self._update_result(vote.index, ident, result, comment).addCallback(done)
                else:
                    def done(*args):
                        self.bot.fire('votum_added', vote.index, call.nick, votum, comment)
                        self._check_vote_status(vote)
                    self._add_result(vote.index, ident, result, comment).addCallback(done)
            #vote._getResults().addCallback(done)
            done(True)
        self._whois(call.nick).addCallback(user_check)

    def voteyes(self, call, args):
        """!vyes <id> [comment]

        Vote 'yes' on the given poll."""

        args.insert(0, 'yes')
        self.vote(call, args)

    def voteno(self, call, args):
        """!vno <id> [comment]

        Vote 'no' on the given poll."""

        args.insert(0, 'no')
        self.vote(call, args)

    def voteabstain(self, call, args):
        """!vabstain <id> [comment]

        Vote 'abstain' on the given poll."""

        args.insert(0, 'abstain')
        self.vote(call, args)

    #------------------------------------------------------------------

    def veto(self, call, args):
        """!veto <id> <comment>

        Put a veto on the given poll (only available to admins). The comment field is mandatory."""
        vote = None
        result = None
        comment = None

        if len(args) < 2:
            call.reply(_("You have to specify the id of the poll you want to put a veto on, and add a comment to your vote!"))
            return

        if len(args) > 0:
            vote_id = int(args[0][1:] if args[0].startswith("#") else args[0])
            try:
                vote = self.voting.votes[vote_id]
            except:
                call.reply(_("Sorry, poll #{0} is not available!").format(vote_id))
                return
        #else:
        #    vote = self._get_active_vote()
        #    if not vote:
        #        call.reply(_("Sorry, there is no poll running right now!"))
        #        return

        # Allow admins to veto polls that have ended - request by Samual
        #if not vote.isActive():
        #    call.reply(_("Sorry, poll #{0} is no longer active!").format(vote_id))
        #    return

        if vote.getStatus() == Vote.VCHALLENGED:
            call.reply(_("Sorry, challenged polls can not be vetoed!"))
            return

        if vote.getStatus() == Vote.VDEPRECATED:
            call.reply(_("Sorry, this poll has already be challenged!"))
            return

        if len(args) >= 2:
            comment = " ".join(args[1:])

        if not comment:
            call.reply(_("Sorry, the comment field is mandatory!"))
            return

        timelimit = _convert_time(config.get("Voting", "vote veto time limit").encode("string-escape"))
        if itime() > vote.getEnd() + timelimit:
            call.reply(_("Sorry, polls can only be vetoed for up to {0} after ending!").format(_timestring(timelimit, fromNow=False)))
            return

        def user_check(success):
            if not success:
                call.reply(_("Sorry, only registered accounts can vote!"))
                return

            def done(*args):
                if self.authnames[call.nick] == vote.creator:
                    call.reply(_("Sorry, as the poll creator you can't vote here!"))
                    return

                if not self.isAdmin(call.nick):
                    call.reply(_("Sorry, only privileged users can add a veto!"))
                    return

                if vote.isActive():
                    d = call.confirm("Do you really want to \02veto\x02 the running poll titled \x02\x0306{0}\x0f?".format(vote.title))
                else:
                    d = call.confirm("Do you really want to \02veto\x02 the already ended poll titled \x02\x0306{0}\x0f?".format(vote.title))
                def _confirmed(ret):
                    if ret:
                        def done(*args):
                            def done(*args):
                                def done(*args):
                                    self.bot.fire('veto_added', vote.index, call.nick, result, comment)
                                    call.reply(_("Done."))
                                #vote._getResults().addCallback(done)
                                done(True)
                            self._update_vote(vote.index, timeout=vote.timeout, end_dt=itime(), status=Vote.VVETOED, result=vote.result).addCallback(done)
                        ident = self.authnames[call.nick]
                        if ident in vote.getVotes():
                            self._update_result(vote.index, ident, VoteResult.VVETO, comment).addCallback(done)
                        else:
                            self._add_result(vote.index, ident, VoteResult.VVETO, comment).addCallback(done)
                    else:
                        call.reply(_("Cancelled."))
                d.addCallback(_confirmed)
            #vote._getResults().addCallback(done)
            done(True)
        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def challenge(self, call, args):
        """!challenge <id>

        Challenge the given poll that was vetoed before (only available to admins)."""
        vote = None

        if len(args) < 1:
            call.reply(_("You have to specify the id of the poll you want to put a veto on!"))
            return

        if len(args) > 0:
            vote_id = int(args[0][1:] if args[0].startswith("#") else args[0])
            try:
                vote = self.voting.votes[vote_id]
            except:
                call.reply(_("Sorry, poll #{0} is not available!").format(vote_id))
                return
        #else:
        #    vote = self._get_active_vote()
        #    if not vote:
        #        call.reply(_("Sorry, there is no poll running right now!"))
        #        return

        if not vote.getStatus() in [ Vote.VVETOED, Vote.VDECIDED ]:
            call.reply(_("Sorry, only vetoed polls and decisions can be challenged!"))
            return

        timelimit = _convert_time(config.get("Voting", "vote challenge time limit").encode("string-escape"))
        if itime() > vote.getEnd() + timelimit:
            call.reply(_("Sorry, polls can only be challenged for up to {0} after ending!").format(_timestring(timelimit, fromNow=False)))
            return

        def user_check(success):
            if not success:
                call.reply(_("Sorry, only registered accounts can vote!"))
                return

            if not self.isAdmin(call.nick):
                call.reply(_("Sorry, only privileged users can challenge polls!"))
                return

            d = call.confirm("Do you really want to \02challenge\x02 the poll titled \x02\x0306{0}\x0f?".format(vote.title))
            def _confirmed(ret):
                #print "confirmed: {0}".format(ret)
                if ret:
                    def done(*args):
                        def done(*args):
                            def done(*args):
                                newvote = self._get_vote(ident=ident, title=vote.title)  # get new vote
                                self.bot.fire('vote_challenged', newvote.index, call.nick, vote.index)
                            self.voting.reload(done)
                        ident = self.authnames[call.nick]
                        self._create_vote(ident, vote.title, status=Vote.VCHALLENGED).addCallback(done)
                    self._update_vote(vote.index, timeout=vote.timeout, end_dt=vote.end_dt, status=Vote.VDEPRECATED, result=vote.result).addCallback(done)
                else:
                    call.reply(_("Cancelled."))
            return d.addCallback(_confirmed)
        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def rename(self, call, args):
        """!rename <id> <title>

        Rename existing poll. Use with care!"""

        if len(args) < 1:
            call.reply(_("You have to specify the id of the poll you want to rename!"))
            return

        if len(args) > 0:
            vote_id = int(args[0][1:] if args[0].startswith("#") else args[0])
            try:
                vote = self.voting.votes[vote_id]
            except:
                call.reply(_("Sorry, poll #{0} is not available!").format(vote_id))
                return

        if not vote.isActive():
            call.reply(_("Sorry, poll #{0} is no longer active!").format(vote_id))
            return

        if len(args) < 2:
            call.reply(_("Sorry, please provide a new title for the given poll!"))
            return

        oldtitle = vote.title
        newtitle = " ".join(args[1:])

        def user_check(success):
            if not success:
                call.reply(_("Sorry, only registered accounts can vote!"))
                return

            if not self.isAdmin(call.nick):
                call.reply(_("Sorry, only privileged users can rename polls!"))
                return

            d = call.confirm("Do you really want to \02rename\x02 the poll titled \x02\x0306{0}\x0f?".format(vote.title))
            def _confirmed(ret):
                if ret:
                    def done(*args):
                        def done(*args):
                            newvote = self._get_vote(ident=vote.creator, title=newtitle)  # get new vote
                            self.bot.fire('vote_renamed', vote.index, oldtitle, newtitle, call.nick)
                        self.voting.reload(done)
                    self._rename_vote(vote.index, title=newtitle).addCallback(done)
                else:
                    call.reply(_("Cancelled."))
            return d.addCallback(_confirmed)
        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def pollURL(self, call, args):
        """!pollurl [id]

        Show URL to xonvote webpage, either for poll list or for specific poll."""
        vote = None

        if len(args) > 0:
            vote_id = int(args[0][1:] if args[0].startswith("#") else args[0])
            try:
                vote = self.voting.votes[vote_id]
            except:
                call.reply(_("Sorry, poll #{0} is not available!").format(vote_id))
                return

        if vote:
            url = "{0}:{1}/poll/{2}".format(config.get("Voting", "http server url"), config.get("Voting", "http server port"), vote.index)
        else:
            url = "{0}:{1}/polls".format(config.get("Voting", "http server url"), config.get("Voting", "http server port"))
        url += "?key={0}".format(config.get("Voting", "access key").decode('string-escape'))

        call.reply(_("Poll URL: {0}").format(url))

    #------------------------------------------------------------------

    def adduser(self, call, args):
        """!adduser [-CORE] <nick>

        Add nick to the list of users allowed to vote (only available to admins). Pass '-CORE' as first argument to make this user gain special privileges.
        The command can also be used to modify users."""
        flags = []

        pargs = []
        for arg in args[:]:
            if arg[0] == "-":
                flags.append( arg[1:].upper() )
            else:
                pargs.append( arg )

        if len(pargs) < 1:
            call.reply(_("Sorry, you have to specify a username!"))
            return

        targetnick = pargs[0]

        make_admin = ( "CORE" in flags )
        skip_usercheck = ( "FORCE" in flags )

        if skip_usercheck:
            if len(pargs) < 2:
                call.reply(_("Sorry, you have to specify a username AND an authname if using the -force option!"))
                return
            targetident = pargs[1]
        else:
            targetident = None

        def user_check(success):
            #print "user_check:", success
            if not success:
                call.reply(_("This command is only available to registered users!"))
                return

            #if not self.isAdmin(call.nick):
            #    call.reply(_("Sorry, you're not allowed to use this command!"))
            #    return

            if not skip_usercheck:
                d = FetchedList.get_users(self.bot, self.bot.channel).get()
                def _fireEvent(userlist):
                    #print "userlist: ", userlist
                    users = {}
                    for nick, ident, host, flags in userlist:
                        users[nick] = ident

                    if not targetnick in users.keys():
                        call.reply(_("Sorry, you can't allow unknown users to vote!"))
                        return

                    def done(success):
                        #print success, self.authnames
                        if not success:
                            call.reply(_("Sorry, can't get info on this user (probably unregistered)!"))
                            return

                        ident = self.authnames[targetnick]

                        if self.isAllowed(targetnick) or self.isAllowedIdent(ident):
                            call.reply(_("Note: This user already exists!"))

                        if make_admin:
                            d = call.confirm("Do you want to allow \x02{0}\x02 (ident {1}) to vote \02with admin rights\02?".format(targetnick, ident))
                        else:
                            d = call.confirm("Do you want to allow \x02{0}\x02 (ident {1}) to vote?".format(targetnick, ident))

                        def _confirmed(ret):
                            if ret:
                                def done(*args):
                                    def done(*args):
                                        self.bot.fire('user_added', targetnick, ident, call.nick)
                                        call.reply(_("Done."))
                                    self.users.reload(done)
                                if make_admin:
                                    status = User.UPRIVILEGED
                                else:
                                    status = User.UNORMAL
                                if self.isAllowedIdent(ident):
                                    self._mod_user(ident, status).addCallback(done)
                                else:
                                    self._add_user(targetnick, ident, status).addCallback(done)
                            else:
                                call.reply(_("Cancelled."))
                        return d.addCallback(_confirmed)
                    self._whois(targetnick).addCallback(done)
                d.addCallback(_fireEvent)
            else:
                # SKIPPING USER CHECKS
                if self.isAllowed(targetnick) or self.isAllowedIdent(targetident):
                    call.reply(_("Note: This user already exists!"))
                    #call.reply(_("Sorry, this user is already allowed to vote!"))
                    #return

                ident = targetident
                if make_admin:
                    d = call.confirm("Do you want to allow \x02{0}\x02 (ident {1}) to vote \02with admin rights\02?".format(targetnick, ident))
                else:
                    d = call.confirm("Do you want to allow \x02{0}\x02 (ident {1}) to vote?".format(targetnick, ident))

                def _confirmed(ret):
                    if ret:
                        def done(*args):
                            def done(*args):
                                self.bot.fire('user_added', targetnick, ident, call.nick)
                                call.reply(_("Done."))
                            self.users.reload(done)
                        if make_admin:
                            status = User.UPRIVILEGED
                        else:
                            status = User.UNORMAL
                        if self.isAllowedIdent(ident):
                            self._mod_user(ident, status).addCallback(done)
                        else:
                            self._add_user(targetnick, ident, status).addCallback(done)
                    else:
                        call.reply(_("Cancelled."))
                return d.addCallback(_confirmed)
        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def deluser(self, call, args):
        """!deluser <nick>

        Remove nick from the list of users allowed to vote (only available to admins).
        The command essentially revokes the right to vote, and using !adduser again allows to make the user valid again."""

        if len(args) < 1:
            call.reply(_("You have to specify a username!"))
            return

        targetnick = args[0]

        def user_check(success):
            if not success:
                call.reply(_("This command is only available to registered users!"))
                return

            #if not self.isAdmin(call.nick):
            #    call.reply(_("Sorry, you're not allowed to use this command!"))
            #    return

            d = FetchedList.get_users(self.bot, self.bot.channel).get()
            def _fireEvent(userlist):
                users = {}
                for nick, ident, host, flags in userlist:
                    users[nick] = ident

                if not targetnick in users.keys():
                    call.reply(_("Sorry, you can't allow unknown users to vote!"))
                    return

                def done(success):
                    if not success:
                        call.reply(_("Sorry, can't get info on this user (probably unregistered)!"))
                        return

                    if not self.isAllowed(targetnick):
                        call.reply(_("Sorry, this user is not allowed to vote yet!"))
                        return

                    ident = self.authnames[targetnick]
                    d = call.confirm("Do you want to remove \x02{0}\x02 (ident {1}) from the userlist?".format(targetnick, ident))
                    def _confirmed(ret):
                        if ret:
                            def done(*args):
                                def done(*args):
                                    self.bot.fire('user_deleted', targetnick, ident, call.nick)
                                    call.reply(_("Done."))
                                self.users.reload(done)
                            #self._del_user(ident).addCallback(done)
                            self._mod_user(ident, status=User.UINVALID).addCallback(done)
                        else:
                            call.reply(_("Cancelled."))
                    return d.addCallback(_confirmed)
                self._whois(targetnick).addCallback(done)
            d.addCallback(_fireEvent)
        self._whois(call.nick).addCallback(user_check)

    #------------------------------------------------------------------

    def listusers(self, call, args):
        """!listusers

        Shows lists of known users allowed to vote."""

        users = self.users.users
        if len(users) == 0:
            call.reply(_("Sorry, there are no users registered yet!"))
            return

        call.reply(_("There are {0} users registered: {1}").\
                format( len(users),
                        ", ".join([ str(user) for user in users.values() ])
                ))

    #==================================================================

    def voteStarted(self, vote_id, nick):
        """new poll started"""
        def done(*args):
            vote = self.voting[vote_id]
            self.lastReminders[vote_id] = itime()
            # use str() here instead of repr(), people deserve to see the full title at least once
            self.bot.cmsg(_("New poll #{1} was \x02started\x02:\x0f {0}").format(str(vote), vote_id))
            self._send_notification_mail(vote_id, "started", is_new=True)
            self._auto_abstain_users(vote_id)
        self.voting.reload(done)

    def voteDecided(self, vote_id, nick):
        """new decision"""
        def done(*args):
            vote = self.voting[vote_id]
            self.lastReminders[vote_id] = itime()
            # use str() here instead of repr(), people deserve to see the full title at least once
            self.bot.cmsg(_("New topic #{1} was \x02decided\x02:\x0f {0}").format(str(vote), vote_id))
            self._send_notification_mail(vote_id, "decided", is_new=True)
            #self._auto_abstain_users(vote_id)
        self.voting.reload(done)

    def voteStopped(self, vote_id, nick):
        """running poll stopped"""
        def done(*args):
            vote = self.voting[vote_id]
            if vote_id in self.lastReminders:
                del self.lastReminders[vote_id]
            self.bot.cmsg(_("Poll #{1} was \x02stopped\x02 by {2}:\x0f {0}").format(repr(vote), vote_id, nick))
            self._send_notification_mail(vote_id, "stopped")
        self.voting.reload(done)

    def votePostponed(self, vote_id, nick):
        """running poll postponed"""
        def done(*args):
            vote = self.voting[vote_id]
            self.lastReminders[vote_id] = itime()
            self.bot.cmsg(_("Poll #{1} was \x02postponed\x02 by {2}:\x0f {0}").format(repr(vote), vote_id, nick))
            self._send_notification_mail(vote_id, "postponed (by {0})".format(nick))
            self.messageOne(vote.creator, _("Your poll #{1} was \x02postponed\x02 by {2}:\x0f {0}.").format(repr(vote), vote_id, nick))
        self.voting.reload(done)

    def voteResumed(self, vote_id, nick):
        """resume postponed poll"""
        def done(*args):
            vote = self.voting[vote_id]
            self.lastReminders[vote_id] = itime()
            self.bot.cmsg(_("Poll #{1} was \x02resumed\x02 by {2}:\x0f {0}").format(repr(vote), vote_id, nick))
            self._send_notification_mail(vote_id, "resumed (by {0})".format(nick))
            self.messageAll(_("Poll #{1} was \x02resumed\x02 by {2}:\x0f {0}.").format(repr(vote), vote_id, nick), only_admins=False)
        self.voting.reload(done)

    def voteChallenged(self, vote_id, nick, former_id):
        """challenge vetoed poll"""
        def done(*args):
            vote = self.voting[vote_id]
            self.lastReminders[vote_id] = itime()
            self.bot.cmsg(_("Poll #{1} (formerly #{3}) was \x02challenged\x02 by {2}:\x0f {0}").format(repr(vote), vote_id, nick, former_id))
            self._send_notification_mail(vote_id, "challenged (by {0})".format(nick), is_new=True)
            self.messageAll(_("Poll #{1} (formerly #{3} was \x02challenged\x02 by {2}:\x0f {0}.").format(repr(vote), vote_id, nick, former_id), only_admins=True)
        self.voting.reload(done)

    def voteRenamed(self, vote_id, oldtitle, newtitle, nick):
        """rename existing poll"""
        def done(*args):
            vote = self.voting[vote_id]
            self.lastReminders[vote_id] = itime()
            self.bot.cmsg(_("Poll #{1} was \x02renamed\x02 by {2}:\x0f {0}").format(repr(vote), vote_id, nick))
            self._send_notification_mail(vote_id, "renamed (by {0})".format(nick))
            self.messageAll(_("Poll #{1}was \x02renamed\x02 by {2}:\x0f {0}.").format(repr(vote), vote_id, nick), only_admins=True)
        self.voting.reload(done)

    def votePassed(self, vote_id, leftusers):
        """poll passed by adding another 'yes' vote"""
        def done(*args):
            vote = self.voting[vote_id]
            if vote_id in self.lastReminders:
                del self.lastReminders[vote_id]
            self.bot.cmsg(_("Poll #{1} \x02passed\x02, {2} didn't have to vote:\x0f {0}").format(repr(vote), vote_id, leftusers))
            self._post_tracker_item(vote_id)  ## ONLY FOR PASSED POLLS!
            self._send_notification_mail(vote_id, "passed")
        self.voting.reload(done)

    def voteFailed(self, vote_id, leftusers):
        """poll passed by adding another 'yes' vote"""
        def done(*args):
            vote = self.voting[vote_id]
            if vote_id in self.lastReminders:
                del self.lastReminders[vote_id]
            self.bot.cmsg(_("Poll #{1} \x02failed\x02, {2} didn't have to vote:\x0f {0}").format(repr(vote), vote_id, leftusers))
            self._send_notification_mail(vote_id, "failed")
        self.voting.reload(done)

    def voteTied(self, vote_id, leftusers):
        """poll tied by adding another vote"""
        def done(*args):
            vote = self.voting[vote_id]
            if vote_id in self.lastReminders:
                del self.lastReminders[vote_id]
            self.bot.cmsg(_("Poll #{1} \x02tied\x02, {2} didn't have to vote:\x0f {0}").format(repr(vote), vote_id, leftusers))
            self._send_notification_mail(vote_id, "tied")
        self.voting.reload(done)

    def voteAutoAbstained(self, vote_id, users):
        """auto-abstained inactive users"""
        def done(*args):
            vote = self.voting[vote_id]
            self.bot.cmsg(_("Auto-abstained {2} users on poll #{1}:\x0f {0}").format(repr(vote), vote_id, len(users)))
            self._check_vote_status(vote)
        self.voting.reload(done)

    def votumAdded(self, vote_id, nick, result, comment):
        """votum added to poll"""
        def done(*args):
            vote = self.voting[vote_id]
            self.bot.cmsg(_("Vote \x02{3}\x02 added to poll #{1} by {2}:\x0f {0}").format(repr(vote), vote_id, nick, result))
        self.voting.reload(done)

    def votumChanged(self, vote_id, nick, oldresult, result, comment):
        """votum changed on poll"""
        def done(*args):
            vote = self.voting[vote_id]
            if oldresult:
                self.bot.cmsg(_("Vote of {2} changed from \x02{3}\x02 to \x02{4}\x02 on poll #{1}:\x0f {0}").format(repr(vote), vote_id, nick, oldresult, result))
            else:
                self.bot.cmsg(_("Vote of {2} changed to \x02{4}\x02 on poll #{1}:\x0f {0}").format(repr(vote), vote_id, nick, result))
        self.voting.reload(done)

    def vetoAdded(self, vote_id, nick, result, comment):
        """veto added to poll"""
        def done(*args):
            vote = self.voting[vote_id]
            self.bot.cmsg(_("{2} put a \x02veto\x02 on poll #{1}:\x0f {0}").format(repr(vote), vote_id, nick))
            self.messageOne(vote.creator, _("Your poll #{1} was vetoed by {2}:\x0f {0}.").format(repr(vote), vote_id, nick))
            self._send_notification_mail(vote_id, "vetoed (by {0})".format(nick))
        self.voting.reload(done)

    def voteTimedout(self, vote_id, leftusers, curtime):
        """running poll timed out"""
        if vote_id in self.reminderShown:
            del self.reminderShown[vote_id]
        def done(*args):
            vote = self.voting[vote_id]
            if vote_id in self.lastReminders:
                del self.lastReminders[vote_id]
            if vote.status == Vote.VTIED:
                msg = _("Poll #{1} \x02timed out (tied)\x02, {2} didn't have to vote:\x0f {0}")
                subject = "timed out (tied)"
            elif vote.status == Vote.VPASSED:
                msg = _("Poll #{1} \x02timed out (passed)\x02, {2} didn't have to vote:\x0f {0}")
                subject = "timed out (passed)"
            elif vote.status == Vote.VFAILED:
                msg = _("Poll #{1} \x02timed out (failed)\x02, {2} didn't have to vote:\x0f {0}")
                subject = "timed out (failed)"
            else:
                msg = _("Poll #{1} \x02timed out\x02, {2} didn't have to vote:\x0f {0}")
                subject = "timed out"
            self.bot.cmsg(msg.format(repr(vote), vote_id, leftusers))
            if vote.status == Vote.VPASSED:
                self._post_tracker_item(vote_id)  ## ONLY FOR PASSED POLLS!
            self._send_notification_mail(vote_id, subject)
        self.voting.reload(done)

    def voteRunningout(self, vote_id, curtime):
        """reminder for running-out poll"""
        if vote_id in self.reminderShown:
            if self.reminderShown[vote_id]:
                return
        def done(*args):
            vote = self.voting[vote_id]
            self.reminderShown[vote_id] = True
            self.lastReminders[vote_id] = curtime
            self.bot.cmsg(_("Poll #{1} is running out soon:\x0f {0}").format(repr(vote), vote_id))
            #self.messageOne(vote.creator, _("Your poll #{1} is running out soon:\x0f {0}.").format(repr(vote), vote_id))
        self.voting.reload(done)

    def voteReminder(self, vote_id, curtime):
        """reminder for still-running poll"""
        def done(*args):
            vote = self.voting[vote_id]
            self.lastReminders[vote_id] = curtime
            self.bot.cmsg(_("Auto-reminder for poll #{1}:\x0f {0}").format(repr(vote), vote_id))
        self.voting.reload(done)

    def globalReminder(self, numvotes, curtime):
        """reminder for running polls"""
        self.lastReminder = curtime
        if numvotes > 0:
            if numvotes == 1:
                msg = _("Reminder: There is 1 poll running")
            else:
                msg = _("Reminder: There are {0} polls running").format(numvotes)
            msg +=  _(" (use \x02!polls\x02 to see a list)\x0f")
            self.bot.cmsg(msg)

    def userAdded(self, nick, ident, creator):
        """user added to voters list"""
        self.bot.cmsg(_("User \x02{0}\x02 (ident {1}) is now allowed to vote").format(nick, ident))

    def userDeleted(self, nick, ident, creator):
        """user deleted from voters list"""
        self.bot.cmsg(_("User \x02{0}\x02 (ident {1}) is no longer allowed to vote").format(nick, ident))

    def userJoined(self, user, channel):
        """track user joins"""
        user = user.split('!')[0]
        def done(result):
            if result:
                if not self.users.isAllowed(user):
                    return
                authname = self.authnames[user]

                activevotes = self._get_running_votes()
                if len(activevotes) > 0:
                    if len(activevotes) == 1:
                        msg = _("There is 1 poll running right now")
                    else:
                        msg = _("There are {} polls running right now").format(len(activevotes))
                    msg += _(" \x0315(Use \x02!polls\x02 to see entire list.)")
                    self.bot.pmsg(user, msg)

                missingvotes = self._get_missing_votes(authname)
                if len(missingvotes) > 0:
                    if len(missingvotes) == 1:
                        msg = _("Your vote is missing in 1 poll:")
                    else:
                        msg = _("Your vote is missing in {} polls:").format(len(missingvotes))
                    msg += _("\x0f {0} \x0315(Use \x02!polls -open\x02 to see entire list, \x02!details <#id>\x02 for details.)").\
                            format(", ".join([ "#{0}".format(v.index) for v in missingvotes ]))
                    self.bot.pmsg(user, msg)

        self._whois(user).addCallback(done)

    def userRenamed(self, oldname, newname):
        """track user renames"""
        if oldname in self.authnames:
            self.authnames[newname] = self.authnames[oldname]
            del self.authnames[oldname]
        pass

    def userLeft(self, user, channel, *args):
        """track quitters"""
        nick = user.split('!')[0]
        if nick in self.authnames:
            del self.authnames[nick]
        pass

    def userQuit(self, user, quitMessage):
        """track quitters"""
        nick = user.split('!')[0]
        if nick in self.authnames:
            del self.authnames[nick]
        pass

    def noticed(self, user, target, message):
        #print user, target, message
        ident = ""
        if ident in self.deferredInfo:
            self.deferredInfo[ident].callback(True)
            del self.deferredInfo[ident]

    def whoisReply(self, user, authname):
        self.authnames[user] = authname
        log.msg("Got whois reply for user {0}: ident is {1}".format(user, authname))
        if user in self.deferredWhois:
            if len(self.deferredWhois[user]) > 0:
                log.msg("Succesfully whois'd {0}".format(user))
                self.deferredWhois[user][0].callback(True)  # use first (oldest) item
                del self.deferredWhois[user][0]
                return
        log.msg("No whois requested for {0}?".format(user))

    def test(self, call, args):
        if call.nick != "zykure":
            call.reply(_("You're welcome!"))
        else:
            self._post_tracker_item( int(args[0]) )
            call.reply(_("Thanks."))

    def rejoining(self, channel, delay):
        self._cleanup()

    def joinedHomeChannel(self):
        self._postInit()


    #==================================================================

    commands = {
        'callvote':     (callvote,      COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
            'vcall':    (callvote,      COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
        'decide':       (decide,        COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
        'stopvote':     (stopvote,      COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
            'vstop':    (stopvote,      COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
        'postpone':     (postpone,      COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
        'resume':       (resume,        COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
        'challenge':    (challenge,     COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
        'info':         (result,        COMMAND.NOT_FROM_OUTSIDE),
            'status':   (result,        COMMAND.NOT_FROM_OUTSIDE),
        'polls':        (votes ,        COMMAND.NOT_FROM_OUTSIDE),
            'votes':    (votes ,        COMMAND.NOT_FROM_OUTSIDE),
        'details':      (details,       COMMAND.NOT_FROM_OUTSIDE),
        'pollurl':      (pollURL,       COMMAND.NOT_FROM_OUTSIDE),
        'vote':         (vote,          COMMAND.NOT_FROM_OUTSIDE),
            'vyes':     (voteyes,       COMMAND.NOT_FROM_OUTSIDE),
            'vno':      (voteno,        COMMAND.NOT_FROM_OUTSIDE),
            'vabstain': (voteabstain,   COMMAND.NOT_FROM_OUTSIDE),
        'veto':         (veto,          COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
        'rename':       (rename,        COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
        'adduser':      (adduser,       COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
        'deluser':      (deluser,       COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
        'listusers':    (listusers,     COMMAND.NOT_FROM_OUTSIDE),

        'test':         (test,          COMMAND.NOT_FROM_OUTSIDE | COMMAND.NOT_FROM_PM),
    }

    eventhandlers = {
        'vote_started':     voteStarted,
        'vote_stopped':     voteStopped,
        'vote_decided':     voteDecided,
        'vote_postponed':   votePostponed,
        'vote_resumed':     voteResumed,
        'vote_challenged':  voteChallenged,
        'vote_renamed':     voteRenamed,
        'vote_timedout':    voteTimedout,
        'vote_runningout':  voteRunningout,
        'vote_reminder':    voteReminder,
        'global_vote_reminder':   globalReminder,
        'vote_passed':      votePassed,
        'vote_failed':      voteFailed,
        'vote_tied':        voteTied,
        'vote_auto_abstained': voteAutoAbstained,
        'votum_added':      votumAdded,
        'votum_changed':    votumChanged,
        'veto_added':       vetoAdded,
        'user_added':       userAdded,
        'user_deleted':     userDeleted,
        'userJoined':       userJoined,
        'userRenamed':      userRenamed,
        'userLeft':         userLeft,
        'userKicked':       userLeft,
        'userQuit':         userQuit,
        'noticed':          noticed,
        'whoisReply_330':   whoisReply,
        'rejoining':        rejoining,
        'joinedHomeChannel': joinedHomeChannel,
    }

voting = SimpleModuleFactory(VoteBot)
