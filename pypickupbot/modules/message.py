# pypickupbot - An ircbot that helps game players to play organized games
#               with captain-picked teams.
#     Copyright (C) 2010 pypickupbot authors
#     Copyright (C) 2012-2017 Jan Behrens <zykure42@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""channel topic management"""

from time import time, sleep

from twisted.python import log
from twisted.internet import defer

from pypickupbot.modable import SimpleModuleFactory
from pypickupbot.irc import COMMAND, InputError, FetchedList
from pypickupbot import db
from pypickupbot import config
from pypickupbot.misc import str_from_timediff, timediff_from_str,\
    StringTypes, itime

class MessageRelay:

    def __init__(self, bot):
        db.runOperation("""
            CREATE TABLE IF NOT EXISTS
            messages
            (
                id      INTEGER PRIMARY KEY AUTOINCREMENT,
                time    INTEGER,
                nick    TEXT,
                target  TEXT,
                content TEXT
            )""")

        self.pypickupbot = bot

    def add_message(self, nick, target, message):
        def _doTransaction(txn, nick, target, message):
            tm = itime()
            txn.execute("""
                INSERT INTO messages(nick, target, content, time)
                VALUES (:nick, :target, :message, :time)
                """, (nick, target, message, tm,))
            return txn.fetchall()
        return db.runInteraction(_doTransaction, nick, target, message)

    def remove_message(self, nick, target):
        def _doTransaction(txn, nick, target):
            txn.execute("""
                DELETE FROM messages
                WHERE nick = ? AND target = ?
                """, (nick, target,))
            return txn.fetchall()
        return db.runInteraction(_doTransaction, nick, target)

    def remove_message_multi(self, targets):
        def _doTransaction(txn, targets):
            for n in targets:
                txn.execute("""
                    DELETE FROM messages
                    WHERE target = ?
                    """, (n,))
            return txn.fetchall()
        return db.runInteraction(_doTransaction, targets)

    def clear_messages(self):
        def _doTransaction(txn):
            txn.execute("""
                DELETE FROM messages
                """)
            return txn.fetchall()
        return db.runInteraction(_doTransaction)

    def update_message(self, nick, target, content):
        d = db.runOperation("""
                UPDATE messages
                SET content = ?
                WHERE nick = ? AND target = ?
                """, (content, nick, target,))
        return d

    def get_messages(self, target):
        d = db.runQuery("""
                SELECT nick, content, time
                FROM messages
                WHERE target = ?""", (target,))
        return d

    def get_message_from(self, nick, target):
        d = db.runQuery("""
                SELECT nick, content, time
                FROM messages
                WHERE nick = ? AND target = ?
                LIMIT 1""", (nick, target,))
        return d

    def joinedHomeChannel(self):
        """when home channel joined, deliver messages"""
        d = FetchedList.get_users(self.pypickupbot, self.pypickupbot.channel).get()
        def _fireEvent(userlist):
            users = []
            for nick, ident, host, flags in userlist:
                users.append(nick)
            self.deliverMessages(users)
        d.addCallback(_fireEvent)

    def userJoined(self, user, channel, *args):
        """when new user joined, deliver messages"""
        nick = user.split('!')[0]
        if channel == self.pypickupbot.channel:
            self.deliverMessages([nick])

    def deliverMessages(self, nicks):
        now = itime()
        log.msg("delivering messages to {}".format(nicks))
        for n in nicks:
            def _printResult(r, target):
                for entry in r:
                    sender, message, time = entry
                    age = str_from_timediff(now - time)
                    self.pypickupbot.pmsg(target, _("Message from {0} ({2}): {1}").format(sender, message, age))
                self.remove_message_multi([target])
            self.get_messages(n).addCallback(_printResult, n)

    def addMessage(self, call, args):
        """!addmsg <to> <message>

        Add a message for the given nick. The user will be notified when joining the channel."""

        if len(args) < 2:
            call.reply(_("You need to specify a nickname and a message."))
            return

        nick = call.nick
        target = args[0]
        content = " ".join(args[1:])

        d = FetchedList.get_users(self.pypickupbot, self.pypickupbot.channel).get()
        def _fireEvent(userlist):
            users = []
            for unick, ident, host, flags in userlist:
                users.append(unick)
            if target in users:
                call.reply(_("User {} is currently in this channel!").format(target))
                return

            def _getResult(r):
                def done(updated, *args):
                    if updated:
                        call.reply(_("Your previous message to {} has been updated.").format(target))
                    else:
                        call.reply(_("Your message to {} has been saved.").format(target))
                if len(r) > 0:
                    self.update_message(nick, target, content).addCallback(done, True)
                else:
                    self.add_message(nick, target, content).addCallback(done, False)
            self.get_message_from(nick, target).addCallback(_getResult)
        d.addCallback(_fireEvent)

    def delMessage(self, call, args):
        """!delmsg <to>

        Delete a message for the given nick that was added earlier."""

        if len(args) < 1:
            call.reply(_("You need to specify a nickname."))
            return

        nick = call.nick
        target = args[0]

        def _getResult(r):
            if len(r) == 0:
                call.reply(_("You have not added any messages to {0}.").format(target))
                return

            def _confirmed(ret):
                if ret:
                    def done(*args):
                        call.reply(_("Done."))
                    self.remove_message(nick, target).addCallback(done)
                else:
                    call.reply(_("Cancelled."))
                    return

            d = call.confirm(
                _("This will delete your message to {0}, continue?").format(target)
            )
            d.addCallback(_confirmed)
        self.get_message_from(nick, target).addCallback(_getResult)

    def clearMessages(self, call, args):
        """!clearnotes [nick1] [nick2] [...]

        Clears the message list for a given user. If no nicks are given, this removes all messages."""

        nicks = args

        if len(nicks) > 0:
            d = call.confirm(_("This will delete messages from {}, continue?").format(", ".join(nicks)))
        else:
            d = call.confirm(_("This will delete all messages, continue?"))

        def _confirmed(ret):
            if ret:
                def done(*args):
                    call.reply(_("Done."))
                if len(nicks) > 0:
                    self.remove_message_multi(nicks).addCallback(done)
                else:
                    self.clear_messages().addCallback(done)
            else:
                call.reply(_("Cancelled."))
        return d.addCallback(_confirmed)

    commands = {
        'note':         (addMessage,    COMMAND.NOT_FROM_OUTSIDE),
        'delnote':      (delMessage,    COMMAND.NOT_FROM_OUTSIDE),

        'clearnotes':   (clearMessages, COMMAND.NOT_FROM_OUTSIDE | COMMAND.ADMIN),
        }
    eventhandlers = {
        'joinedHomeChannel':    joinedHomeChannel,
        'userJoined':           userJoined,
        }

message_relay = SimpleModuleFactory(MessageRelay)
