# pypickupbot - An ircbot that helps game players to play organized games
#               with captain-picked teams.
#     Copyright (C) 2010 pypickupbot authors
#     Copyright (C) 2012-2017 Jan Behrens <zykure42@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from time import time
from datetime import datetime
import json

from twisted.internet import defer, reactor
from twisted.python import log
from twisted.web import server, resource

from pypickupbot.modable import SimpleModuleFactory
from pypickupbot import db
from pypickupbot import config
from pypickupbot.irc import COMMAND, InputError
from pypickupbot.misc import str_from_timediff, timediff_from_str,\
    InvalidTimeDiffString, StringTypes, itime

class HttpQuery(resource.Resource):

    isLeaf = True

    def __init__(self, bot):

        self.site = server.Site(self)
        reactor.listenTCP(int(config.get("HTTP Query", "port").decode('string-escape')), self.site)

        try:
                self.pickup = bot.load('xonstat_pickup')
        except:
                self.pickup = None

        try:
                self.gameserver = bot.load('gameserver')
        except:
                self.gameserver = None

        self.pypickupbot = bot

    def _getPlayers(self):
        players = {}
        if self.pickup:
            for gt,game in self.pickup.games.items():
                for p in game.players:
                    if not p in players:
                        nick = self.pickup.xonstat._get_original_nick(p).lower()
                        if nick in self.pickup.xonstat.players:
                            player = self.pickup.xonstat.players[nick]
                        else:
                            player = self.pickup.Player(p, None)

                        players[p] = {
                            'nick': player.nick,
                            'playerid': player.playerid,
                            'games': [gt],
                        }
                    players[p]['games'].append(gt)
        return players

    def _getGames(self, forceall=False):
        games = {}
        if self.pickup:
            for gt,game in self.pickup.games.items():
                if forceall or len(game.players) > 0:
                    games[gt] = {
                            'nick': game.nick,
                            'name': game.name,
                            'numcaptains': game.caps,
                            'maxplayers': game.maxplayers,
                            'numplayers': len(game.players),
                            'players': game.players,
                            'starting': game.starting,
                            'autopick': game.autopick,
                            'info': game.info,
                            'teamnames': game.teamnames,
                        }
        return games

    def _getServers(self):
        servers = {}
        if self.gameserver:
            for name,server in self.gameserver.servers.servers.items():
                if server.is_available():
                    if server.is_full():
                        status = "full"
                    elif server.is_empty():
                        status = "empty"
                    else:
                        status = "free"
                else:
                    status = "down"
                servers[name] = {
                        'status': status,
                        'name': server.get_fullname(),
                        'ip': server.get_ip(),
                        'ping': server.get_ping(),
                        'num_players': server.num_players(),
                        'max_players': server.max_players(),
                        'num_spectators': server.num_spectators(),
                        'location': server.get_location(),
                        'current_map': server.get_map(),
                        'version': server.get_gameversion(),
                        'mod': server.get_gamemod(),
                        'gametype': server.get_gametype(),
                }
        return servers

    def render_GET(self, request):
        #print request
        result = {}
        if request.uri == "/":
                result['status'] = "okay"

        elif request.uri == "/status":
                result['status'] = "okay"
                result['activegames'] = self._getGames()
                result['players'] = self._getPlayers()

        elif request.uri == "/games":
                result['status'] = "okay"
                result['games'] = self._getGames(forceall=True)

        elif request.uri == "/servers":
                result['status'] = "okay"
                result['servers'] = self._getServers()

        else:
                result['status'] = "not implemented"

        return str([result])

    def query(self, call, args):
        self.render_GET("GET /" + "/".join(args))

    commands = {
        'query':        (query,         0),
        }
    eventhandlers = {
        #'pickup_game_starting': pickup_game_started,
        }

http_query = SimpleModuleFactory(HttpQuery)

